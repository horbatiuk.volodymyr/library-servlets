<main class="container">
    <h1><fmt:message key="login.enter.credentials"/></h1>
    <form action="<c:url value="/login"/>" method="post">
        <div class="mb-3">
            <label for="username" class="form-label"><fmt:message key="login.login"/></label>
            <input type="text" class="form-control" id="username" name="username" autofocus required>
        </div>
        <div class="mb-3">
            <label for="password" class="form-label"><fmt:message key="login.password"/></label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <c:if test="${loginError}"><p class="alert alert-danger"><fmt:message key="login.error"/></p></c:if>
        <button type="submit" class="btn btn-primary">
            <fmt:message key="login.submit"/>
        </button>
    </form>
</main>
