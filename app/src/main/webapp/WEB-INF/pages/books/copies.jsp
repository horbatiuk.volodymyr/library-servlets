<main class="container">
    <h1><fmt:message key="books.available.copies"/></h1>

    <c:if test="${empty bookCopies}">
        <div class="alert alert-light" role="alert">
            <fmt:message key="books.available.copies.no.copies.available"/></div>
    </c:if>

    <c:if test="${not empty bookCopies}">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><fmt:message key="books.available.book.title"/></th>
                <th scope="col"><fmt:message key="books.available.book.authors"/></th>
                <th scope="col"><fmt:message key="books.available.book.copy.inventory.number"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="bookCopy" items="${bookCopies}" varStatus="bookCopyStat">
                <tr>
                    <th scope="row"><c:out value="${bookCopyStat.count}"/></th>
                    <td><c:out value="${bookCopy.book.title}"/></td>
                    <td><c:out value="${bookCopy.book.author}"/></td>
                    <td><c:out value="${bookCopy.inventoryNum}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <form action="<c:url value="/users/${user.id}/requests"/>" method="post">
            <c:set var="bookId" value="${bookCopies.get(0).book.id}"/>
            <div class="input-group">
                <label hidden for="loan_type_name" class="form-label">
                    <fmt:message key="books.available.book.copy.loan.type"/></label>
                <select id="loan_type_name" name="loan_type_name" class="form-select">
                    <option selected value="reading_room">
                        <fmt:message key="books.available.book.copy.loan.type.reading.room"/>
                    </option>
                    <option value="home">
                        <fmt:message key="books.available.book.copy.loan.type.home"/>
                    </option>
                </select>
                <input type="hidden" name="book_id" value="${bookId}">
                <button type="submit" class="btn btn-outline-primary btn-sm">
                    <fmt:message key="books.available.book.copy.request"/>
                </button>
            </div>
        </form>
    </c:if>
</main>
