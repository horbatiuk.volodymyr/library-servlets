<main class="container">
    <h1><fmt:message key="admin.users.table.name"/></h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><fmt:message key="admin.users.table.user.first.name"/></th>
            <th scope="col"><fmt:message key="admin.users.table.user.last.name"/></th>
            <th scope="col"><fmt:message key="admin.users.table.user.login"/></th>
            <th scope="col"><fmt:message key="admin.users.table.user.enabled"/></th>
            <th scope="col"><fmt:message key="admin.users.table.user.enabled.change"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="user" items="${users}" varStatus="userStat">
            <tr>
                <th scope="row"><c:out value="${userStat.count}"/></th>
                <td><c:out value="${user.firstName}"/></td>
                <td><c:out value="${user.lastName}"/></td>
                <td><c:out value="${user.login}"/></td>
                <td><fmt:message key="admin.users.table.user.enabled.${user.enabled}"/></td>
                <td>
                    <form action="<c:url value="/admin/"/>" method="post">
                        <input type="hidden" id="user_id" name="user_id" value="${user.id}">
                        <input type="hidden" id="enabled" name="enabled" value="${!user.enabled}">
                        <button type="submit" class="btn btn-outline-primary btn-sm">
                            <fmt:message key="admin.users.table.user.enabled.change.${user.enabled}"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</main>
