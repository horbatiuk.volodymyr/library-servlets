<main class="container">
    <h1><fmt:message key="user.books.request"/></h1>
    <p><fmt:message key="user.books.request.book.title"/>: <c:out value="${bookRequest.book.title}"/></p>
    <p><fmt:message key="user.books.request.book.authors"/>: <c:out value="${bookRequest.book.author}"/></p>
    <p><fmt:message key="user.books.request.book.loan.type"/>:
        <c:choose>
            <c:when test="${'home'.equalsIgnoreCase(bookRequest.loanType.name)}">
                <fmt:message key="user.books.request.book.loan.type.home"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="user.books.request.book.loan.type.reading.room"/>
            </c:otherwise>
        </c:choose>
    </p>
    <p><fmt:message key="user.books.request.satisfied"/>:
        <fmt:message key="user.books.request.satisfied.${bookRequest.satisfied}"/></p>
</main>
