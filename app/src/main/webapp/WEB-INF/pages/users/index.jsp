<%@ taglib prefix="lib" tagdir="/WEB-INF/tags" %>
<main class="container">
    <h1><fmt:message key="user.main.loaned.books"/></h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><fmt:message key="user.loaned.books.book.title"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.authors"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.loan.type"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.inventory.number"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.issue.date"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.due.date"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.return.date"/></th>
            <th scope="col"><fmt:message key="user.loaned.books.book.overdue.fine"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="bookLoan" items="${bookLoans}" varStatus="bookLoanStat">
            <tr>
                <th scope="row"><c:out value="${bookLoanStat.count}"/></th>
                <td><c:out value="${bookLoan.bookTitle}"/></td>
                <td><c:out value="${bookLoan.bookAuthor}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${'home'.equalsIgnoreCase(bookLoan.loanType.name)}">
                            <fmt:message key="user.loaned.books.book.loan.type.home"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="user.loaned.books.book.loan.type.reading.room"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td><c:out value="${bookLoan.bookInventoryNum}"/></td>
                <c:choose>
                    <c:when test="${locale eq 'ru'}">
                        <c:set var="datePattern" value="dd.MM.yyyy"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="datePattern" value="MM/dd/yyyy"/>
                    </c:otherwise>
                </c:choose>
                <td><lib:localDate date="${bookLoan.issueDate}" pattern="${datePattern}"/></td>
                <td><lib:localDate date="${bookLoan.dueDate}" pattern="${datePattern}"/></td>
                <td><lib:localDate date="${bookLoan.returnDate}" pattern="${datePattern}"/></td>
                <td><c:out value="${bookLoan.totalOverdueFine}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</main>
