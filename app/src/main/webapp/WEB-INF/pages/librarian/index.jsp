<main class="container">
    <h1><fmt:message key="librarian.requests"/></h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><fmt:message key="librarian.requests.book.title"/></th>
            <th scope="col"><fmt:message key="librarian.requests.book.authors"/></th>
            <th scope="col"><fmt:message key="librarian.requests.loan.type"/></th>
            <th scope="col"><fmt:message key="librarian.requests.user"/></th>
            <th scope="col"><fmt:message key="librarian.requests.satisfy"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="request" items="${openRequests}" varStatus="requestStat">
            <tr>
                <th scope="row"><c:out value="${requestStat.count}"/></th>
                <td><c:out value="${request.book.title}"/></td>
                <td><c:out value="${request.book.author}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${'home'.equalsIgnoreCase(request.loanType.name)}">
                            <fmt:message key="user.loaned.books.book.loan.type.home"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="user.loaned.books.book.loan.type.reading.room"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td><c:out value="${request.user.firstName} ${request.user.lastName}"/></td>
                <td>
                    <form action="<c:url value="/librarian/"/>" method="post">
                        <input type="hidden" id="request_id" name="request_id" value="${request.id}">
                        <button type="submit" class="btn btn-outline-primary btn-sm">
                            <fmt:message key="librarian.requests.satisfy"/>
                        </button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</main>
