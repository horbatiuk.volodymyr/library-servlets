<%@ page import="horbatiuk.library.controller.SortBy" %>
<main class="container">
    <h1><fmt:message key="main.search.find.book"/></h1>
    <form action="<c:url value="/"/>" method="get">
        <div class="row g-3 align-items-center">
            <div class="col">
                <label hidden for="query" class="form-label">
                    <fmt:message key="main.search.placeholder" var="mainSearchPlaceholder"/></label>
                <input type="search" name="query" id="query" class="form-control"
                       value="${not empty searchParams['query'] ? searchParams['query'] : ''}"
                       placeholder="${mainSearchPlaceholder}">
            </div>
            <div class="col-auto">
                <label hidden for="sort_by" class="form-label">
                    <fmt:message key="main.search.sort.by"/></label>
                <select id="sort_by" name="sort_by" class="form-select">
                    <c:set var="selectedSortByCode" value="${searchParams['sort_by'].code}"/>
                    <c:set var="valuesOfSortBy" value="<%=SortBy.values()%>"/>
                    <c:forEach var="sortBy" items="${valuesOfSortBy}">
                        <option value="${sortBy.code}" ${selectedSortByCode == sortBy.code ? 'selected' : ''}>
                            <fmt:message key="main.search.sort.option.${sortBy.code}"/>
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-auto">
                <label for="page_size" class="col-form-label">
                    <fmt:message key="main.search.results.per.page"/></label>
            </div>
            <div class="col-auto">
                <select id="page_size" name="page_size" class="form-select">
                    <c:set var="selectedPageSize" value="${searchParams['page_size']}"/>
                    <option ${selectedPageSize == 5 ? 'selected' : ''} value="5">5</option>
                    <option ${selectedPageSize == 10 ? 'selected' : ''} value="10">10</option>
                </select>
            </div>
            <div class="col-auto">
                <button class="btn btn-outline-success" type="submit">
                    <fmt:message key="main.search.button"/></button>
            </div>
        </div>
    </form>

    <c:if test="${empty books}">
        <div class="alert alert-light" role="alert">
            <fmt:message key="main.search.results.nothing.was.found"/></div>
    </c:if>

    <c:if test="${not empty books}">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><fmt:message key="main.search.results.title"/></th>
                <th scope="col"><fmt:message key="main.search.results.authors"/></th>
                <th scope="col"><fmt:message key="main.search.results.publisher"/></th>
                <th scope="col"><fmt:message key="main.search.results.year"/></th>
                <th scope="col"><fmt:message key="main.search.results.request"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="book" items="${books}" varStatus="bookStat">
                <tr>
                    <th scope="row"><c:out value="${bookStat.count}"/></th>
                    <td><c:out value="${book.title}"/></td>
                    <td><c:out value="${book.author}"/></td>
                    <td><c:out value="${book.publisher.name}"/></td>
                    <td><c:out value="${book.year}"/></td>
                    <td>
                        <form action="<c:url value="/books/${book.id}/copies"/>" method="get">
                            <label hidden for="available" class="form-label">
                                <fmt:message key="books.available.copies"/></label>
                            <input type="hidden" id="available" name="available" value="true">
                            <button type="submit" class="btn btn-outline-primary btn-sm">
                                <fmt:message key="main.search.results.request"/></button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <c:set var="pageNumbers" value="${searchParams['page_numbers']}"/>
    <c:if test="${pageNumbers.size() > 0}">
        <nav>
            <ul class="pagination">
                <c:forEach var="pageNumber" items="${pageNumbers}">
                    <li class="page-item${pageNumber == searchParams['required_page']? ' active' : ''}">
                        <c:url var="pageNumberUrl" value="">
                            <c:param name="query" value="${searchParams['query']}"/>
                            <c:param name="sort_by" value="${searchParams['sort_by'].code}"/>
                            <c:param name="page_size" value="${searchParams['page_size']}"/>
                            <c:param name="required_page" value="${pageNumber}"/>
                        </c:url>
                        <a class="page-link" href="${pageNumberUrl}">
                            <c:out value="${pageNumber}"/></a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </c:if>
</main>
