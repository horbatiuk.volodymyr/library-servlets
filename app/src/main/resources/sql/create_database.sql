DROP DATABASE IF EXISTS library;
CREATE DATABASE library;
USE library;


CREATE TABLE `user`
(
    id         INT AUTO_INCREMENT PRIMARY KEY,
    login      VARCHAR(255) NOT NULL UNIQUE,
    password   VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    enabled    BOOL         NOT NULL DEFAULT FALSE
) ENGINE = InnoDB;


CREATE TABLE role
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
) ENGINE = InnoDB;


CREATE TABLE user_role
(
    user_id INT NOT NULL,
    role_id INT NOT NULL,
    CONSTRAINT user_role_pk PRIMARY KEY (user_id, role_id),
    CONSTRAINT user_role__user_id_fk FOREIGN KEY (user_id) REFERENCES `user` (id),
    CONSTRAINT user_role__role_id_fk FOREIGN KEY (role_id) REFERENCES role (id)
) ENGINE = InnoDB;


CREATE TABLE publisher
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
) ENGINE = InnoDB;


CREATE TABLE book
(
    id           INT AUTO_INCREMENT PRIMARY KEY,
    title        VARCHAR(255) NOT NULL,
    author       VARCHAR(255) NOT NULL,
    year         YEAR         NOT NULL,
    publisher_id INT          NOT NULL,
    INDEX title_ix (title),
    INDEX author_ix (author),
    INDEX year_ix (year),
    CONSTRAINT book__publisher_id_fk FOREIGN KEY (publisher_id) REFERENCES publisher (id),
    CONSTRAINT UNIQUE book_title_author_publisher_year_uk (title, author, publisher_id, year)
) ENGINE = InnoDB;


CREATE TABLE book_copy
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    book_id       INT         NOT NULL,
    inventory_num VARCHAR(15) NOT NULL UNIQUE,
    CONSTRAINT book_copy__book_id_fk FOREIGN KEY (book_id) REFERENCES book (id)
) ENGINE = InnoDB;


CREATE TABLE loan_type
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(25) NOT NULL UNIQUE
) ENGINE = InnoDB;


CREATE TABLE book_request
(
    id           INT AUTO_INCREMENT PRIMARY KEY,
    satisfied    BOOL NOT NULL DEFAULT FALSE,
    book_id      INT  NOT NULL,
    loan_type_id INT  NOT NULL,
    user_id      INT  NOT NULL,
    CONSTRAINT book_request__book_id_fk FOREIGN KEY (book_id) REFERENCES book (id),
    CONSTRAINT book_request__loan_type_id_fk FOREIGN KEY (loan_type_id) REFERENCES loan_type (id),
    CONSTRAINT book_request__user_id_fk FOREIGN KEY (user_id) REFERENCES `user` (id)
) ENGINE = InnoDB;


CREATE TABLE book_loan
(
    id           INT AUTO_INCREMENT PRIMARY KEY,
    book_copy_id INT  NOT NULL,
    loan_type_id INT  NOT NULL,
    user_id      INT  NOT NULL,
    issue_date   DATE NOT NULL,
    due_date     DATE NOT NULL,
    return_date  DATE,
    CONSTRAINT book_loan__book_copy_id_fk FOREIGN KEY (book_copy_id) REFERENCES book_copy (id),
    CONSTRAINT book_loan__user_id_fk FOREIGN KEY (user_id) REFERENCES `user` (id),
    CONSTRAINT book_loan__loan_type_id_fk FOREIGN KEY (loan_type_id) REFERENCES loan_type (id)
) ENGINE = InnoDB;


DROP USER IF EXISTS 'lib_user'@'localhost';
CREATE USER 'lib_user'@'localhost' IDENTIFIED BY 'lib_password';
GRANT SELECT, INSERT, UPDATE, DELETE ON library.* TO 'lib_user'@'localhost';
