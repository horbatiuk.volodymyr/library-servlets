INSERT INTO `user`
VALUES (1, 'kseniia_morozova',
        '$argon2i$v=19$m=16,t=2,p=1$asxl2dePOcOO8Aj5rn1aDQ$m8Xftyh+YGgg3Tmo0O5mwiQ+7z53iH4TvYKBlMxyK2Y',
        'Ксения', 'Морозова', true),
       (2, 'veronika_utkina',
        '$argon2i$v=19$m=16,t=2,p=1$UaDinCqcFgAHvgNNQ8fokg$bmFT9Y+vCeU6RdHCV8H8wQ3hXzlcr3Aidg8eVtXeBwE',
        'Вероника', 'Уткина', true),
       (3, 'ivan_kovalchuk',
        '$argon2i$v=19$m=16,t=2,p=1$zl9KwndsUbwr9Y3LSSjBJg$LaZNhV6wWfYjmjvxsmBWTA2eM6ua3uMQr/Tur5brrwQ',
        'Иван', 'Ковальчук', true),
       (4, 'artem_belchuk',
        '$argon2i$v=19$m=16,t=2,p=1$K3z/fPIROYpCCZuWA12oAg$AiX81MT3FjpM07i9/l05XQgRj7Qk7EjyYTj7Wm1tqgY',
        'Артём', 'Бельчук', true),
       (5, 'olesia_kirienko',
        '$argon2i$v=19$m=16,t=2,p=1$eDS+TMbOII3VZnrvwy3glg$A1SzdDGM3vKN1YtFW76qv5CTZ8IcPAXQFHAhJHnDU9Q',
        'Олеся', 'Кириенко', true),
       (6, 'aleksei_sorokin',
        '$argon2i$v=19$m=16,t=2,p=1$MoGN9wvX1ZTu8VUQcMSUlQ$nVPRuogm4p0fPkF3oH3SQmjJPbHj3KPpZ6Ti/xdXbMc',
        'Алексей', 'Сорокин', true),
       (7, 'daria_mironova',
        '$argon2i$v=19$m=16,t=2,p=1$te6LgohS+/CshWgvTzKwEw$xETMew5RTiNWh7g6oB1SEiXzeMSN9+hVTYdqM/SO2wc',
        'Дарья', 'Миронова', true),
       (8, 'artur_zakharov',
        '$argon2i$v=19$m=16,t=2,p=1$SJAaJuWZNWA6ZN1T1hEvmw$7gHXyt3TVGqZHh/+ezOOHxT5htuQTIs2ISC69+xoNyA',
        'Артур', 'Захаров', false),
       (9, 'iuliia_golubeva',
        '$argon2i$v=19$m=16,t=2,p=1$FzU/ialc7RYrrTxIbgqthQ$MV8cvEtVlFCAlUgSETisp6ZBZqmF1wXmCGLKNL1c1uc',
        'Юлия', 'Голубева', false);

INSERT INTO role
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_LIBRARIAN'),
       (3, 'ROLE_READER');

INSERT INTO user_role
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (2, 2),
       (2, 3),
       (3, 3),
       (4, 3),
       (5, 3),
       (6, 3),
       (7, 3),
       (8, 3),
       (9, 3);

INSERT INTO publisher
VALUES (1, 'Эксмо'),
       (2, 'Питер'),
       (3, 'Символ-Плюс'),
       (4, 'И.Д. Вильямс'),
       (5, 'Новое знание'),
       (6, 'Addison-Wesley'),
       (7, 'Четыре четверти');

INSERT INTO book
VALUES (1, 'Изучаем Java', 'Кэти Сьерра, Берт Бейтс', 2012, 1),
       (2, 'Паттерны проектирования', 'Эрик Фримен, Элизабет Фримен, Кэти Сьерра, Берт Бейтс', 2011, 2),
       (3, 'Изучаем HTML, CSS и XHTML', 'Эрик Фримен, Элизабет Фримен', 2012, 2),
       (4, 'Изучаем SQL', 'Линн Бейли', 2012, 2),
       (5, 'Изучаем XML', 'Эрик Рэй', 2001, 3),
       (6, 'Изучаем программирование на HTML5', 'Эрик Фримен, Элизабет Робсон', 2013, 2),
       (7, 'Язык запросов SQL', 'Филипп Андон, Валерий Резниченко', 2006, 2),
       (8, 'Алгоритмы для начинающих: теория и практика для разработчика', 'Панос Луридас', 2018, 1),
       (9, 'Алгоритмы на Java', 'Роберт Седжвик, Кевин Уэйн', 2013, 4),
       (10, 'Структуры данных и алгоритмы в Java', 'Майкл Гудрич, Роберто Тамассия', 2003, 5),
       (11, 'Объектно-ориентированный анализ и проектирование', 'Бретт Маклафлин, Гарри Поллайс, Давид Уэст', 2013, 2),
       (12, 'Java concurrency на практике', 'Брайан Гетц, Джошуа Блох', 2020, 2),
       (13, 'Effective Java. Second edition', 'Джошуа Блох', 2008, 6),
       (14, 'Рефакторинг: улучшение существующего кода', 'Мартин Фаулер', 2003, 3),
       (15, 'Чистый код: создание, анализ и рефакторинг', 'Роберт Мартин', 2010, 2),
       (16, 'Совершенный код. Мастер-класс', 'Стив Макконнел', 2005, 2),
       (17, 'Ремесло программиста. Практика написания хорошего кода', 'Питер Гудлиф', 2009, 3),
       (18, 'Экстремальное программирование. Разработка через тестирование', 'Кент Бек', 2003, 2),
       (19, 'Применение UML и шаблонов проектирования', 'Крэг Ларман', 2004, 4),
       (20, 'Java. Новое поколение разработки', 'Бенджамин Эванс, Мартин Вербург', 2014, 2),
       (21, 'Философия Java', 'Брюс Эккель', 2012, 2),
       (22, 'Java. Библиотека профессионала. Том 1. Основы', 'Кей Хорстман', 2019, 4),
       (23, 'Java. Библиотека профессионала. Том 2. Расширенные средства программирования', 'Кей Хорстман', 2020, 4),
       (24, 'Структуры данных и алгоритмы Java. Второе издание', 'Роберт Лафоре', 2011, 2),
       (25, 'Java. Методы программирования', 'Игорь Блинов, Валерий Романчик', 2013, 7);

INSERT INTO book_copy
VALUES (1, 1, 'AБ37-B85-001000'),
       (2, 1, 'AБ37-B85-001001'),
       (3, 1, 'AБ37-B85-001002'),
       (4, 2, 'AБ37-B85-002000'),
       (5, 2, 'AБ37-B85-002001'),
       (6, 2, 'AБ37-B85-002002'),
       (7, 3, 'AБ37-B85-003000'),
       (8, 3, 'AБ37-B85-003001'),
       (9, 3, 'AБ37-B85-003002'),
       (10, 4, 'AБ37-B85-004000'),
       (11, 4, 'AБ37-B85-004001'),
       (12, 4, 'AБ37-B85-004002'),
       (13, 5, 'AБ37-B85-005000'),
       (14, 5, 'AБ37-B85-005001'),
       (15, 5, 'AБ37-B85-005002'),
       (16, 6, 'AБ37-B85-006000'),
       (17, 6, 'AБ37-B85-006001'),
       (18, 6, 'AБ37-B85-006002'),
       (19, 7, 'AБ37-B85-007000'),
       (20, 7, 'AБ37-B85-007001'),
       (21, 7, 'AБ37-B85-007002'),
       (22, 8, 'AБ37-B85-008000'),
       (23, 8, 'AБ37-B85-008001'),
       (24, 8, 'AБ37-B85-008002'),
       (25, 9, 'AБ37-B85-009000'),
       (26, 9, 'AБ37-B85-009001'),
       (27, 9, 'AБ37-B85-009002'),
       (28, 10, 'AБ37-B85-010000'),
       (29, 10, 'AБ37-B85-010001'),
       (30, 10, 'AБ37-B85-010002'),
       (31, 11, 'AБ37-B85-011000'),
       (32, 11, 'AБ37-B85-011001'),
       (33, 11, 'AБ37-B85-011002'),
       (34, 12, 'AБ37-B85-012000'),
       (35, 12, 'AБ37-B85-012001'),
       (36, 12, 'AБ37-B85-012002'),
       (37, 13, 'AБ37-B85-013000'),
       (38, 13, 'AБ37-B85-013001'),
       (39, 13, 'AБ37-B85-013002'),
       (40, 14, 'AБ37-B85-014000'),
       (41, 14, 'AБ37-B85-014001'),
       (42, 14, 'AБ37-B85-014002'),
       (43, 15, 'AБ37-B85-015000'),
       (44, 15, 'AБ37-B85-015001'),
       (45, 15, 'AБ37-B85-015002'),
       (46, 16, 'AБ37-B85-016000'),
       (47, 16, 'AБ37-B85-016001'),
       (48, 16, 'AБ37-B85-016002'),
       (49, 17, 'AБ37-B85-017000'),
       (50, 17, 'AБ37-B85-017001'),
       (51, 17, 'AБ37-B85-017002'),
       (52, 18, 'AБ37-B85-018000'),
       (53, 18, 'AБ37-B85-018001'),
       (54, 18, 'AБ37-B85-018002'),
       (55, 19, 'AБ37-B85-019000'),
       (56, 19, 'AБ37-B85-019001'),
       (57, 19, 'AБ37-B85-019002'),
       (58, 20, 'AБ37-B85-020000'),
       (59, 20, 'AБ37-B85-020001'),
       (60, 20, 'AБ37-B85-020002'),
       (61, 21, 'AБ37-B85-021000'),
       (62, 21, 'AБ37-B85-021001'),
       (63, 21, 'AБ37-B85-021002'),
       (64, 22, 'AБ37-B85-022000'),
       (65, 22, 'AБ37-B85-022001'),
       (66, 22, 'AБ37-B85-022002'),
       (67, 23, 'AБ37-B85-023000'),
       (68, 23, 'AБ37-B85-023001'),
       (69, 23, 'AБ37-B85-023002'),
       (70, 24, 'AБ37-B85-024000'),
       (71, 24, 'AБ37-B85-024001'),
       (72, 24, 'AБ37-B85-024002'),
       (73, 25, 'AБ37-B85-025000'),
       (74, 25, 'AБ37-B85-025001'),
       (75, 25, 'AБ37-B85-025002');

INSERT INTO loan_type
VALUES (1, 'READING_ROOM'),
       (2, 'HOME');

INSERT INTO book_request
VALUES (1, true, 11, 2, 1),
       (2, true, 9, 1, 1),
       (3, true, 6, 1, 1),
       (4, true, 8, 2, 1),
       (5, true, 10, 1, 1),
       (6, true, 23, 2, 1),
       (7, false, 19, 1, 1),
       (8, false, 20, 2, 1),
       (9, true, 16, 1, 2),
       (10, true, 18, 1, 2),
       (11, true, 9, 1, 2),
       (12, true, 25, 1, 2),
       (13, true, 15, 2, 2),
       (14, true, 14, 2, 2),
       (15, false, 7, 2, 2),
       (16, true, 18, 2, 3),
       (17, true, 25, 2, 3),
       (18, true, 9, 2, 3),
       (19, true, 17, 2, 4),
       (20, true, 10, 1, 4),
       (21, true, 6, 2, 4),
       (22, true, 1, 1, 4),
       (23, true, 22, 2, 4),
       (24, true, 24, 2, 4),
       (25, true, 7, 1, 4),
       (26, true, 23, 2, 4),
       (27, true, 3, 1, 4),
       (28, false, 19, 1, 4),
       (29, false, 16, 2, 4),
       (30, false, 4, 1, 4),
       (31, true, 5, 1, 5),
       (32, true, 7, 2, 5),
       (33, true, 20, 2, 5),
       (34, true, 17, 2, 5),
       (35, true, 14, 2, 5),
       (36, true, 16, 1, 5),
       (37, true, 15, 1, 5),
       (38, true, 6, 2, 6),
       (39, true, 13, 1, 6),
       (40, true, 19, 1, 6),
       (41, true, 13, 2, 7),
       (42, true, 18, 1, 7),
       (43, true, 22, 2, 8),
       (44, true, 24, 2, 8),
       (45, true, 23, 2, 8),
       (46, true, 20, 1, 8),
       (47, true, 14, 2, 8),
       (48, true, 1, 1, 8),
       (49, true, 21, 1, 8),
       (50, true, 7, 2, 8),
       (51, true, 16, 2, 8),
       (52, false, 3, 1, 8),
       (53, false, 19, 2, 8);

INSERT INTO book_loan
VALUES (1, 31, 2, 1, '2017-01-16', '2017-01-30', '2017-01-27'),
       (2, 25, 1, 1, '2017-10-06', '2017-10-06', '2017-10-06'),
       (3, 16, 1, 1, '2018-09-21', '2018-09-21', '2018-09-21'),
       (4, 22, 2, 1, '2019-03-22', '2019-04-05', '2019-04-05'),
       (5, 28, 1, 1, '2020-03-31', '2020-03-31', '2020-03-31'),
       (6, 67, 2, 1, '2021-04-04', '2021-04-18', '2021-04-18'),
       (7, 46, 1, 2, '2017-02-14', '2017-02-14', '2017-02-14'),
       (8, 52, 1, 2, '2018-03-13', '2018-03-13', '2018-03-13'),
       (9, 26, 1, 2, '2019-04-05', '2019-04-05', '2019-04-05'),
       (10, 73, 1, 2, '2019-12-10', '2019-12-10', '2019-12-10'),
       (11, 43, 2, 2, '2020-05-05', '2020-05-19', '2020-05-15'),
       (12, 40, 2, 2, '2021-01-17', '2021-01-31', '2021-01-31'),
       (13, 53, 2, 3, '2020-01-22', '2020-02-05', '2020-02-05'),
       (14, 74, 2, 3, '2020-03-16', '2020-03-30', '2020-04-03'),
       (15, 27, 2, 3, '2021-03-15', '2021-03-29', '2021-03-29'),
       (16, 49, 2, 4, '2017-03-06', '2017-03-20', '2017-03-15'),
       (17, 29, 1, 4, '2017-11-07', '2017-11-07', '2017-11-07'),
       (18, 17, 2, 4, '2018-02-15', '2018-03-01', '2018-02-28'),
       (19, 1, 1, 4, '2019-04-08', '2019-04-08', '2019-04-08'),
       (20, 64, 2, 4, '2019-08-05', '2019-08-19', '2019-08-19'),
       (21, 70, 2, 4, '2019-12-12', '2019-12-26', '2019-12-27'),
       (22, 19, 1, 4, '2020-05-08', '2020-05-08', '2020-05-08'),
       (23, 68, 2, 4, '2020-10-19', '2020-11-02', '2020-10-26'),
       (24, 7, 1, 4, '2021-05-17', '2021-05-17', '2021-05-17'),
       (25, 13, 1, 5, '2017-04-14', '2017-04-14', '2017-04-14'),
       (26, 20, 2, 5, '2018-06-21', '2018-07-05', '2018-07-06'),
       (27, 58, 2, 5, '2018-07-06', '2018-07-20', '2018-07-17'),
       (28, 50, 2, 5, '2019-06-11', '2019-06-25', '2019-06-27'),
       (29, 41, 2, 5, '2020-07-23', '2020-08-06', '2020-08-06'),
       (30, 47, 1, 5, '2021-03-24', '2021-03-24', '2021-03-24'),
       (31, 44, 1, 5, '2021-05-18', '2021-05-18', '2021-05-18'),
       (32, 18, 2, 6, '2017-05-30', '2017-06-13', '2017-06-13'),
       (33, 37, 1, 6, '2019-03-05', '2019-03-05', '2019-03-05'),
       (34, 55, 1, 6, '2021-05-19', '2021-05-19', '2021-05-19'),
       (35, 38, 2, 7, '2020-11-16', '2020-11-30', '2020-11-30'),
       (36, 54, 1, 7, '2021-04-20', '2021-04-20', '2021-04-20'),
       (37, 65, 2, 8, '2017-08-07', '2017-08-21', '2017-08-16'),
       (38, 71, 2, 8, '2017-09-01', '2017-09-15', '2017-09-15'),
       (39, 69, 2, 8, '2018-07-02', '2018-07-16', '2018-07-31'),
       (40, 59, 1, 8, '2018-08-13', '2018-08-13', '2018-08-13'),
       (41, 42, 2, 8, '2019-06-18', '2019-07-02', '2019-07-01'),
       (42, 2, 1, 8, '2019-12-04', '2019-12-04', '2019-12-04'),
       (43, 61, 1, 8, '2020-06-05', '2020-06-05', '2020-06-05'),
       (44, 21, 2, 8, '2021-02-08', '2021-02-22', '2021-02-22'),
       (45, 48, 2, 8, '2021-04-07', '2021-04-21', '2021-04-18');
