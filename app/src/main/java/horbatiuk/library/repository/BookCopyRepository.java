package horbatiuk.library.repository;

import horbatiuk.library.entity.BookCopy;

import java.util.List;

public interface BookCopyRepository extends GenericRepository<BookCopy> {

    List<BookCopy> findByBookId(int bookId);
}
