package horbatiuk.library.repository;

import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.BookLoan;

import java.util.List;

public interface BookLoanRepository extends GenericRepository<BookLoan> {

    List<BookLoan> findByBookCopyInAndReturnDateIsNull(List<BookCopy> copies);

    List<BookLoan> findByUserId(int userId);
}
