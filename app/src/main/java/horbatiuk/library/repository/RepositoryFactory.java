package horbatiuk.library.repository;

public interface RepositoryFactory {

    BookCopyRepository getBookCopyRepository();

    BookLoanRepository getBookLoanRepository();

    BookRepository getBookRepository();

    BookRequestRepository getBookRequestRepository();

    LoanTypeRepository getLoanTypeRepository();

    PublisherRepository getPublisherRepository();

    RoleRepository getRoleRepository();

    UserRepository getUserRepository();
}
