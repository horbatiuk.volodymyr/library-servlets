package horbatiuk.library.repository;

import horbatiuk.library.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends GenericRepository<User> {

    @Override
    Optional<User> findById(int userId);

    Optional<User> findByLogin(String login);

    List<User> findAll();
}
