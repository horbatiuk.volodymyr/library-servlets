package horbatiuk.library.repository;

import horbatiuk.library.entity.Role;

import java.util.List;

public interface RoleRepository extends GenericRepository<Role> {

    List<Role> findByUserId(int userId);
}
