package horbatiuk.library.repository;

import java.util.Optional;

public interface GenericRepository<T> {

    Optional<T> findById(int id);

    T save(T entity);
}
