package horbatiuk.library.repository;

import java.util.List;
import java.util.Objects;

public class Page<T> {

    private final List<T> content;
    private final int totalPages;

    public Page(List<T> content, int totalPages) {
        this.content = Objects.requireNonNull(content);
        if (totalPages < 0) {
            throw new IllegalArgumentException("totalPages must be >= 0");
        }
        this.totalPages = totalPages;
    }

    public List<T> getContent() {
        return content;
    }

    public int getTotalPages() {
        return totalPages;
    }
}
