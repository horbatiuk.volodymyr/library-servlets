package horbatiuk.library.repository;

import horbatiuk.library.entity.BookRequest;

import java.util.List;

public interface BookRequestRepository extends GenericRepository<BookRequest> {

    List<BookRequest> findBySatisfied(boolean satisfied);
}
