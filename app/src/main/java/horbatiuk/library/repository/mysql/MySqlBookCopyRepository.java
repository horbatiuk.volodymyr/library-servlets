package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.repository.BookCopyRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class MySqlBookCopyRepository implements BookCopyRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String BOOK_ID_COLUMN = "book_id";
    private static final String INVENTORY_NUM_COLUMN = "inventory_num";

    private final DataSource dataSource;

    public MySqlBookCopyRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<BookCopy> findById(int bookCopyId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book_copy WHERE id = ?")) {
            ps.setInt(1, bookCopyId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildBookCopy(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book copy with id {}", bookCopyId, e);
        }
        return Optional.empty();
    }

    private BookCopy buildBookCopy(ResultSet resultSet) throws SQLException {
        BookCopy bookCopy = new BookCopy();
        bookCopy.setId(resultSet.getInt(ID_COLUMN));
        bookCopy.setBookId(resultSet.getInt(BOOK_ID_COLUMN));
        bookCopy.setInventoryNum(resultSet.getString(INVENTORY_NUM_COLUMN));
        return bookCopy;
    }

    @Override
    public List<BookCopy> findByBookId(int bookId) {
        List<BookCopy> bookCopies = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book_copy WHERE book_id = ?")) {
            ps.setInt(1, bookId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    bookCopies.add(buildBookCopy(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book copies with book_id {}", bookId, e);
        }
        return bookCopies;
    }

    @Override
    public BookCopy save(BookCopy bookCopy) {
        if (bookCopy.isNew()) {
            return create(bookCopy);
        } else {
            return update(bookCopy);
        }
    }

    private BookCopy create(BookCopy bookCopy) {
        String sql = "INSERT INTO book_copy (book_id, inventory_num) VALUES (?, ?)";
        Optional<BookCopy> createdBookCopy = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setValuesToPreparedStatement(ps, bookCopy);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdBookCopy = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new book copy with book_id {}", bookCopy.getBookId(), e);
        }
        return createdBookCopy
                .orElseThrow(() -> new IllegalStateException("Cannot create book copy"));
    }

    private void setValuesToPreparedStatement(PreparedStatement ps, BookCopy bookCopy) throws SQLException {
        ps.setInt(1, bookCopy.getBookId());
        ps.setString(2, bookCopy.getInventoryNum());
    }

    private BookCopy update(BookCopy bookCopy) {
        String sql = "UPDATE book_copy SET book_id = ?, inventory_num = ? WHERE id = ?";
        Optional<BookCopy> updatedBookCopy = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            setValuesToPreparedStatement(ps, bookCopy);
            ps.setInt(3, bookCopy.getId());
            ps.executeUpdate();
            updatedBookCopy = findById(bookCopy.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update book copy with id {}", bookCopy.getId(), e);
        }
        return updatedBookCopy
                .orElseThrow(() -> new IllegalStateException("Cannot update book copy"));
    }
}
