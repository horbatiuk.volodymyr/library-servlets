package horbatiuk.library.repository.mysql;

import horbatiuk.library.repository.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MySqlRepositoryFactory implements RepositoryFactory {

    private final DataSource dataSource;

    public MySqlRepositoryFactory() {
        try {
            Context context = (Context) new InitialContext().lookup("java:comp/env");
            dataSource = (DataSource) context.lookup("jdbc/libraryDataSource");
        } catch (NamingException e) {
            throw new IllegalStateException("Cannot create dataSource", e);
        }
    }

    @Override
    public BookCopyRepository getBookCopyRepository() {
        return new MySqlBookCopyRepository(dataSource);
    }

    @Override
    public BookLoanRepository getBookLoanRepository() {
        return new MySqlBookLoanRepository(dataSource);
    }

    @Override
    public BookRepository getBookRepository() {
        return new MySqlBookRepository(dataSource);
    }

    @Override
    public BookRequestRepository getBookRequestRepository() {
        return new MySqlBookRequestRepository(dataSource);
    }

    @Override
    public LoanTypeRepository getLoanTypeRepository() {
        return new MySqlLoanTypeRepository(dataSource);
    }

    @Override
    public PublisherRepository getPublisherRepository() {
        return new MySqlPublisherRepository(dataSource);
    }

    @Override
    public RoleRepository getRoleRepository() {
        return new MySqlRoleRepository(dataSource);
    }

    @Override
    public UserRepository getUserRepository() {
        return new MySqlUserRepository(dataSource);
    }
}
