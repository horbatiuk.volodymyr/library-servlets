package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.Book;
import horbatiuk.library.repository.BookRepository;
import horbatiuk.library.repository.Page;
import horbatiuk.library.service.PageRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class MySqlBookRepository implements BookRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String TITLE_COLUMN = "title";
    private static final String AUTHOR_COLUMN = "author";
    private static final String YEAR_COLUMN = "year";
    private static final String PUBLISHER_ID_COLUMN = "publisher_id";
    private static final String PERCENT = "%";
    private static final String EXCEPTION_HAPPENED_DURING_SQL_QUERY_EXECUTION = "An exception happened during SQL query '{}' execution";

    private final DataSource dataSource;

    public MySqlBookRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<Book> findById(int bookId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book WHERE id = ?")) {
            ps.setInt(1, bookId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildBook(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book with id {}", bookId, e);
        }
        return Optional.empty();
    }

    @Override
    public Page<Book> findByTitleContainingOrAuthorContaining(String partOfTitle,
                                                              String partOfAuthor,
                                                              PageRequest pageRequest) {
        List<Book> books;
        switch (pageRequest.getSortBy()) {
            case TITLE:
                books = findByTitleOrAuthorSortedByTitle(partOfTitle, partOfAuthor, pageRequest);
                break;
            case AUTHOR:
                books = findByTitleOrAuthorSortedByAuthor(partOfTitle, partOfAuthor, pageRequest);
                break;
            case YEAR:
                books = findByTitleOrAuthorSortedByYear(partOfTitle, partOfAuthor, pageRequest);
                break;
            case PUBLISHER:
                books = findByTitleOrAuthorSortedByPublisher(partOfTitle, partOfAuthor, pageRequest);
                break;
            default:
                throw new IllegalStateException("Unknown SortBy value: " + pageRequest.getSortBy());
        }
        int totalPages = getTotalPagesForFindByTitleOrAuthor(partOfTitle, partOfAuthor, pageRequest);
        return new Page<>(books, totalPages);
    }

    private List<Book> findByTitleOrAuthorSortedByTitle(String partOfTitle,
                                                        String partOfAuthor,
                                                        PageRequest pageRequest) {
        return findByTitleOrAuthorSorted("SELECT * FROM book WHERE title LIKE ? OR author LIKE ?"
                + " ORDER BY title LIMIT ?, ?", partOfTitle, partOfAuthor, pageRequest);
    }

    private List<Book> findByTitleOrAuthorSortedByAuthor(String partOfTitle,
                                                         String partOfAuthor,
                                                         PageRequest pageRequest) {
        return findByTitleOrAuthorSorted("SELECT * FROM book WHERE title LIKE ? OR author LIKE ?"
                + " ORDER BY author LIMIT ?, ?", partOfTitle, partOfAuthor, pageRequest);
    }

    private List<Book> findByTitleOrAuthorSortedByYear(String partOfTitle,
                                                       String partOfAuthor,
                                                       PageRequest pageRequest) {
        return findByTitleOrAuthorSorted("SELECT * FROM book WHERE title LIKE ? OR author LIKE ?"
                + " ORDER BY year LIMIT ?, ?", partOfTitle, partOfAuthor, pageRequest);
    }

    private List<Book> findByTitleOrAuthorSortedByPublisher(String partOfTitle,
                                                            String partOfAuthor,
                                                            PageRequest pageRequest) {
        String sql = "SELECT * FROM book"
                + " JOIN publisher ON book.publisher_id = publisher.id"
                + " WHERE title LIKE ? OR author LIKE ?"
                + " ORDER BY publisher.name LIMIT ?, ?";
        return findByTitleOrAuthorSorted(sql, partOfTitle, partOfAuthor, pageRequest);
    }

    private List<Book> findByTitleOrAuthorSorted(String sql, String partOfTitle,
                                                 String partOfAuthor,
                                                 PageRequest pageRequest) {
        List<Book> books = new ArrayList<>();
        int rowCount = pageRequest.getSize();
        int offset = pageRequest.getPage() * rowCount;

        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, PERCENT + partOfTitle + PERCENT);
            ps.setString(2, PERCENT + partOfAuthor + PERCENT);
            ps.setInt(3, offset);
            ps.setInt(4, rowCount);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    books.add(buildBook(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find books with SQL query '{}', part of title '{}',"
                            + ", part of author '{}', offset {} and rowCount {}",
                    sql, partOfTitle, partOfAuthor, offset, rowCount, e);
        }
        return books;
    }

    private int getTotalPagesForFindByTitleOrAuthor(String partOfTitle,
                                                    String partOfAuthor,
                                                    PageRequest pageRequest) {
        int rowCount = 0;
        String sql = "SELECT COUNT(*) FROM book WHERE title LIKE ? OR author LIKE ?";
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, PERCENT + partOfTitle + PERCENT);
            ps.setString(2, PERCENT + partOfAuthor + PERCENT);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    rowCount = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOG.warn(EXCEPTION_HAPPENED_DURING_SQL_QUERY_EXECUTION, sql, e);
        }

        int pageSize = pageRequest.getSize();
        return calculateNumberOfPages(rowCount, pageSize);
    }

    private int calculateNumberOfPages(int rowCount, int pageSize) {
        if (rowCount == 0) {
            return rowCount;
        }
        return (rowCount + pageSize - 1) / pageSize;
    }

    @Override
    public Page<Book> findAll(PageRequest pageRequest) {
        List<Book> books;
        switch (pageRequest.getSortBy()) {
            case TITLE:
                books = findAllBooksSortedByTitle(pageRequest);
                break;
            case AUTHOR:
                books = findAllBooksSortedByAuthor(pageRequest);
                break;
            case YEAR:
                books = findAllBooksSortedByYear(pageRequest);
                break;
            case PUBLISHER:
                books = findAllBooksSortedByPublisher(pageRequest);
                break;
            default:
                throw new IllegalStateException("Unknown SortBy value: " + pageRequest.getSortBy());
        }
        int totalPages = getTotalPagesForFindAll(pageRequest);
        return new Page<>(books, totalPages);
    }

    private List<Book> findAllBooksSortedByTitle(PageRequest pageRequest) {
        return findAllBooksSorted("SELECT * FROM book ORDER BY title LIMIT ?, ?", pageRequest);
    }

    private List<Book> findAllBooksSortedByAuthor(PageRequest pageRequest) {
        return findAllBooksSorted("SELECT * FROM book ORDER BY author LIMIT ?, ?", pageRequest);
    }

    private List<Book> findAllBooksSortedByYear(PageRequest pageRequest) {
        return findAllBooksSorted("SELECT * FROM book ORDER BY year LIMIT ?, ?", pageRequest);
    }

    private List<Book> findAllBooksSortedByPublisher(PageRequest pageRequest) {
        String sql = "SELECT * FROM book"
                + " JOIN publisher ON book.publisher_id = publisher.id"
                + " ORDER BY publisher.name LIMIT ?, ?";
        return findAllBooksSorted(sql, pageRequest);
    }

    private List<Book> findAllBooksSorted(String sql, PageRequest pageRequest) {
        List<Book> books = new ArrayList<>();
        int rowCount = pageRequest.getSize();
        int offset = pageRequest.getPage() * rowCount;

        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, offset);
            ps.setInt(2, rowCount);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    books.add(buildBook(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find books with SQL query '{}', offset {} and rowCount {}",
                    sql, offset, rowCount, e);
        }
        return books;
    }

    private Book buildBook(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        book.setId(resultSet.getInt(ID_COLUMN));
        book.setTitle(resultSet.getString(TITLE_COLUMN));
        book.setAuthor(resultSet.getString(AUTHOR_COLUMN));
        Date yearAsSqlDate = resultSet.getDate(YEAR_COLUMN);
        book.setYear(Year.of(yearAsSqlDate.toLocalDate().getYear()));
        book.setPublisherId(resultSet.getInt(PUBLISHER_ID_COLUMN));
        return book;
    }

    private int getTotalPagesForFindAll(PageRequest pageRequest) {
        int rowCount = 0;
        String sql = "SELECT COUNT(*) FROM book";
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {
            if (rs.next()) {
                rowCount = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOG.warn(EXCEPTION_HAPPENED_DURING_SQL_QUERY_EXECUTION, sql, e);
        }

        int pageSize = pageRequest.getSize();
        return calculateNumberOfPages(rowCount, pageSize);
    }

    @Override
    public Book save(Book book) {
        if (book.isNew()) {
            return create(book);
        } else {
            return update(book);
        }
    }

    private Book create(Book book) {
        String sql = "INSERT INTO book (title, author, year, publisher_id) VALUES (?, ?, ?, ?)";
        Optional<Book> createdBook = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setValuesToPreparedStatement(ps, book);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdBook = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new book", e);
        }
        return createdBook
                .orElseThrow(() -> new IllegalStateException("Cannot create book"));
    }

    private void setValuesToPreparedStatement(PreparedStatement ps, Book book) throws SQLException {
        ps.setString(1, book.getTitle());
        ps.setString(2, book.getAuthor());
        LocalDate year = LocalDate.of(book.getYear().getValue(), 1, 1);
        ps.setDate(3, Date.valueOf(year));
        ps.setInt(4, book.getPublisherId());
    }

    private Book update(Book book) {
        String sql = "UPDATE book SET title = ?, author = ?, year = ?, publisher_id = ? WHERE id = ?";
        Optional<Book> updatedBook = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            setValuesToPreparedStatement(ps, book);
            ps.setInt(5, book.getId());
            ps.executeUpdate();
            updatedBook = findById(book.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update book with id {}", book.getId(), e);
        }
        return updatedBook
                .orElseThrow(() -> new IllegalStateException("Cannot update book"));
    }
}
