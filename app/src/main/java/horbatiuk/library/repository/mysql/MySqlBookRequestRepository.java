package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.BookRequest;
import horbatiuk.library.repository.BookRequestRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class MySqlBookRequestRepository implements BookRequestRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String SATISFIED_COLUMN = "satisfied";
    private static final String BOOK_ID_COLUMN = "book_id";
    private static final String LOAN_TYPE_ID_COLUMN = "loan_type_id";
    private static final String USER_ID_COLUMN = "user_id";

    private final DataSource dataSource;

    public MySqlBookRequestRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<BookRequest> findById(int bookRequestId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book_request WHERE id = ?")) {
            ps.setInt(1, bookRequestId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildBookRequest(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book request with id {}", bookRequestId, e);
        }
        return Optional.empty();
    }

    private BookRequest buildBookRequest(ResultSet resultSet) throws SQLException {
        BookRequest bookRequest = new BookRequest();
        bookRequest.setId(resultSet.getInt(ID_COLUMN));
        bookRequest.setSatisfied(resultSet.getBoolean(SATISFIED_COLUMN));
        bookRequest.setBookId(resultSet.getInt(BOOK_ID_COLUMN));
        bookRequest.setLoanTypeId(resultSet.getInt(LOAN_TYPE_ID_COLUMN));
        bookRequest.setUserId(resultSet.getInt(USER_ID_COLUMN));
        return bookRequest;
    }

    @Override
    public BookRequest save(BookRequest bookRequest) {
        if (bookRequest.isNew()) {
            return create(bookRequest);
        } else {
            return update(bookRequest);
        }
    }

    private BookRequest create(BookRequest bookRequest) {
        String sql = "INSERT INTO book_request (satisfied, book_id, loan_type_id, user_id)"
                + " VALUES (?, ?, ?, ?)";
        Optional<BookRequest> createdBookRequest = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setValuesToPreparedStatement(ps, bookRequest);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdBookRequest = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new book request with book_id {}", bookRequest.getBookId(), e);
        }
        return createdBookRequest
                .orElseThrow(() -> new IllegalStateException("Cannot create book request"));
    }

    private void setValuesToPreparedStatement(PreparedStatement ps, BookRequest bookRequest) throws SQLException {
        ps.setBoolean(1, bookRequest.isSatisfied());
        ps.setInt(2, bookRequest.getBookId());
        ps.setInt(3, bookRequest.getLoanTypeId());
        ps.setInt(4, bookRequest.getUserId());
    }

    private BookRequest update(BookRequest bookRequest) {
        String sql = "UPDATE book_request SET satisfied = ?, book_id = ?, loan_type_id = ?, user_id = ?"
                + " WHERE id = ?";
        Optional<BookRequest> updatedBookRequest = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            setValuesToPreparedStatement(ps, bookRequest);
            ps.setInt(5, bookRequest.getId());
            ps.executeUpdate();
            updatedBookRequest = findById(bookRequest.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update book request with id {}", bookRequest.getId(), e);
        }
        return updatedBookRequest
                .orElseThrow(() -> new IllegalStateException("Cannot update book request"));
    }

    @Override
    public List<BookRequest> findBySatisfied(boolean satisfied) {
        List<BookRequest> bookRequests = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book_request WHERE satisfied = ?")) {
            ps.setBoolean(1, satisfied);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    bookRequests.add(buildBookRequest(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book requests with satisfied {}", satisfied, e);
        }
        return bookRequests;
    }
}
