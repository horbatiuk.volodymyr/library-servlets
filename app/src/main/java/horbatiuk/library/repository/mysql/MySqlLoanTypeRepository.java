package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.LoanType;
import horbatiuk.library.repository.LoanTypeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Objects;
import java.util.Optional;

public class MySqlLoanTypeRepository implements LoanTypeRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";

    private final DataSource dataSource;

    public MySqlLoanTypeRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<LoanType> findById(int loanTypeId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM loan_type WHERE id = ?")) {
            ps.setInt(1, loanTypeId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildLoanType(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find loan type with id {}", loanTypeId, e);
        }
        return Optional.empty();
    }

    private LoanType buildLoanType(ResultSet resultSet) throws SQLException {
        LoanType loanType = new LoanType();
        loanType.setId(resultSet.getInt(ID_COLUMN));
        loanType.setName(resultSet.getString(NAME_COLUMN));
        return loanType;
    }

    @Override
    public Optional<LoanType> findByName(String name) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM loan_type WHERE name = ?")) {
            ps.setString(1, name);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildLoanType(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find loan type with name '{}'", name, e);
        }
        return Optional.empty();
    }

    @Override
    public LoanType save(LoanType loanType) {
        if (loanType.isNew()) {
            return create(loanType);
        } else {
            return update(loanType);
        }
    }

    private LoanType create(LoanType loanType) {
        String sql = "INSERT INTO loan_type (name) VALUES (?)";
        Optional<LoanType> createdLoanType = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, loanType.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdLoanType = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new loan type with name {}", loanType.getName(), e);
        }
        return createdLoanType
                .orElseThrow(() -> new IllegalStateException("Cannot create loan type"));
    }

    private LoanType update(LoanType loanType) {
        String sql = "UPDATE loan_type SET name = ? WHERE id = ?";
        Optional<LoanType> updatedLoanType = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, loanType.getName());
            ps.setInt(2, loanType.getId());
            ps.executeUpdate();
            updatedLoanType = findById(loanType.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update loan type with id {}", loanType.getId(), e);
        }
        return updatedLoanType
                .orElseThrow(() -> new IllegalStateException("Cannot update loan type"));
    }
}
