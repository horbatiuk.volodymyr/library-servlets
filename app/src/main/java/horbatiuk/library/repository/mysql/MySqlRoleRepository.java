package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.Role;
import horbatiuk.library.repository.RoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class MySqlRoleRepository implements RoleRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";

    private final DataSource dataSource;

    public MySqlRoleRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<Role> findById(int roleId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM role WHERE id = ?")) {
            ps.setInt(1, roleId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildRole(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find role with id {}", roleId, e);
        }
        return Optional.empty();
    }

    private Role buildRole(ResultSet resultSet) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getInt(ID_COLUMN));
        role.setName(resultSet.getString(NAME_COLUMN));
        return role;
    }

    @Override
    public Role save(Role role) {
        if (role.isNew()) {
            return create(role);
        } else {
            return update(role);
        }
    }

    private Role create(Role role) {
        String sql = "INSERT INTO role (name) VALUES (?)";
        Optional<Role> createdRole = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, role.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdRole = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new role with name {}", role.getName(), e);
        }
        return createdRole
                .orElseThrow(() -> new IllegalStateException("Cannot create role"));
    }

    private Role update(Role role) {
        String sql = "UPDATE role SET name = ? WHERE id = ?";
        Optional<Role> updatedRole = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, role.getName());
            ps.setInt(2, role.getId());
            ps.executeUpdate();
            updatedRole = findById(role.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update role with id {}", role.getId(), e);
        }
        return updatedRole
                .orElseThrow(() -> new IllegalStateException("Cannot update role"));
    }

    @Override
    public List<Role> findByUserId(int userId) {
        List<Role> roles = new ArrayList<>();
        String sql = "SELECT role.id, role.name FROM role"
                + " INNER JOIN user_role ON role.id = user_role.role_id AND user_role.user_id = ?";
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    roles.add(buildRole(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find roles for user_id {}", userId, e);
        }
        return roles;
    }
}
