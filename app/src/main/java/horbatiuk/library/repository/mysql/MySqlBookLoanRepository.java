package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.BaseEntity;
import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.repository.BookLoanRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class MySqlBookLoanRepository implements BookLoanRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String BOOK_COPY_ID_COLUMN = "book_copy_id";
    private static final String LOAN_TYPE_ID_COLUMN = "loan_type_id";
    private static final String USER_ID_COLUMN = "user_id";
    private static final String ISSUE_DATE_COLUMN = "issue_date";
    private static final String DUE_DATE_COLUMN = "due_date";
    private static final String RETURN_DATE_COLUMN = "return_date";

    private final DataSource dataSource;

    public MySqlBookLoanRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<BookLoan> findById(int bookLoanId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book_loan WHERE id = ?")) {
            ps.setInt(1, bookLoanId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildBookLoan(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book loan with id {}", bookLoanId, e);
        }
        return Optional.empty();
    }

    private BookLoan buildBookLoan(ResultSet resultSet) throws SQLException {
        BookLoan bookLoan = new BookLoan();
        bookLoan.setId(resultSet.getInt(ID_COLUMN));
        bookLoan.setBookCopyId(resultSet.getInt(BOOK_COPY_ID_COLUMN));
        bookLoan.setLoanTypeId(resultSet.getInt(LOAN_TYPE_ID_COLUMN));
        bookLoan.setUserId(resultSet.getInt(USER_ID_COLUMN));
        bookLoan.setIssueDate(resultSet.getDate(ISSUE_DATE_COLUMN).toLocalDate());
        bookLoan.setDueDate(resultSet.getDate(DUE_DATE_COLUMN).toLocalDate());
        bookLoan.setReturnDate(Optional.ofNullable(resultSet.getDate(RETURN_DATE_COLUMN))
                .map(Date::toLocalDate)
                .orElse(null));
        return bookLoan;
    }

    @Override
    public List<BookLoan> findByBookCopyInAndReturnDateIsNull(List<BookCopy> copies) {
        List<BookLoan> bookLoans = new ArrayList<>();
        String placeholders = String.join(", ", Collections.nCopies(copies.size(), "?"));
        String sql = String.format("SELECT * FROM book_loan WHERE book_copy_id IN (%s)"
                + " AND return_date IS NULL", placeholders);

        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            int index = 1;
            for (BookCopy bookCopy : copies) {
                ps.setInt(index, bookCopy.getId());
                index++;
            }
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    bookLoans.add(buildBookLoan(rs));
                }
            }
        } catch (SQLException e) {
            List<Integer> bookCopyIds = copies.stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toList());
            LOG.warn("Cannot find book loans for book copy ids {} and empty return date", bookCopyIds, e);
        }
        return bookLoans;
    }

    @Override
    public List<BookLoan> findByUserId(int userId) {
        List<BookLoan> bookLoans = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM book_loan WHERE user_id = ?")) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    bookLoans.add(buildBookLoan(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find book loans with user_id {}", userId, e);
        }
        return bookLoans;
    }

    @Override
    public BookLoan save(BookLoan bookLoan) {
        if (bookLoan.isNew()) {
            return create(bookLoan);
        } else {
            return update(bookLoan);
        }
    }

    private BookLoan create(BookLoan bookLoan) {
        String sql = "INSERT INTO book_loan (book_copy_id, loan_type_id, user_id, issue_date, due_date, return_date)"
                + " VALUES (?, ?, ?, ?, ?, ?)";
        Optional<BookLoan> createdBookLoan = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setValuesToPreparedStatement(ps, bookLoan);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdBookLoan = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new book loan with book_copy_id {}", bookLoan.getBookCopyId(), e);
        }
        return createdBookLoan
                .orElseThrow(() -> new IllegalStateException("Cannot create book loan"));
    }

    private void setValuesToPreparedStatement(PreparedStatement ps, BookLoan bookLoan) throws SQLException {
        ps.setInt(1, bookLoan.getBookCopyId());
        ps.setInt(2, bookLoan.getLoanTypeId());
        ps.setInt(3, bookLoan.getUserId());
        ps.setDate(4, Date.valueOf(bookLoan.getIssueDate()));
        ps.setDate(5, Date.valueOf(bookLoan.getDueDate()));
        Date returnDate = Optional.ofNullable(bookLoan.getReturnDate())
                .map(Date::valueOf)
                .orElse(null);
        ps.setDate(6, returnDate);
    }

    private BookLoan update(BookLoan bookLoan) {
        String sql = "UPDATE book_loan SET book_copy_id = ?, loan_type_id = ?, user_id = ?"
                + ", issue_date = ?, due_date = ?, return_date = ?"
                + " WHERE id = ?";
        Optional<BookLoan> updatedBookLoan = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            setValuesToPreparedStatement(ps, bookLoan);
            ps.setInt(7, bookLoan.getId());
            ps.executeUpdate();
            updatedBookLoan = findById(bookLoan.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update book loan with id {}", bookLoan.getId(), e);
        }
        return updatedBookLoan
                .orElseThrow(() -> new IllegalStateException("Cannot update book loan"));
    }
}
