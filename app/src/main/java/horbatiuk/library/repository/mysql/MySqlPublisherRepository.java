package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.Publisher;
import horbatiuk.library.repository.PublisherRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Objects;
import java.util.Optional;

public class MySqlPublisherRepository implements PublisherRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";

    private final DataSource dataSource;

    public MySqlPublisherRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<Publisher> findById(int publisherId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM publisher WHERE id = ?")) {
            ps.setInt(1, publisherId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Publisher publisher = new Publisher();
                    publisher.setId(rs.getInt(ID_COLUMN));
                    publisher.setName(rs.getString(NAME_COLUMN));
                    return Optional.of(publisher);
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find publisher with id {}", publisherId, e);
        }
        return Optional.empty();
    }

    @Override
    public Publisher save(Publisher publisher) {
        if (publisher.isNew()) {
            return create(publisher);
        } else {
            return update(publisher);
        }
    }

    private Publisher create(Publisher publisher) {
        String sql = "INSERT INTO publisher (name) VALUES (?)";
        Optional<Publisher> createdPublisher = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, publisher.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdPublisher = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new publisher with name {}", publisher.getName(), e);
        }
        return createdPublisher
                .orElseThrow(() -> new IllegalStateException("Cannot create publisher"));
    }

    private Publisher update(Publisher publisher) {
        String sql = "UPDATE publisher SET name = ? WHERE id = ?";
        Optional<Publisher> updatedPublisher = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, publisher.getName());
            ps.setInt(2, publisher.getId());
            ps.executeUpdate();
            updatedPublisher = findById(publisher.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update publisher with id {}", publisher.getId(), e);
        }
        return updatedPublisher
                .orElseThrow(() -> new IllegalStateException("Cannot update publisher"));
    }
}
