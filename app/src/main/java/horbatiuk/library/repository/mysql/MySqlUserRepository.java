package horbatiuk.library.repository.mysql;

import horbatiuk.library.entity.User;
import horbatiuk.library.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class MySqlUserRepository implements UserRepository {

    private static final Logger LOG = LogManager.getLogger();
    private static final String ID_COLUMN = "id";
    private static final String LOGIN_COLUMN = "login";
    private static final String PASSWORD_COLUMN = "password";
    private static final String FIRST_NAME_COLUMN = "first_name";
    private static final String LAST_NAME_COLUMN = "last_name";
    private static final String ENABLED_COLUMN = "enabled";
    private static final String ROLE_ID_COLUMN = "role_id";

    private final DataSource dataSource;

    public MySqlUserRepository(DataSource dataSource) {
        this.dataSource = Objects.requireNonNull(dataSource);
    }

    @Override
    public Optional<User> findById(int userId) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM user WHERE id = ?")) {
            ps.setInt(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildUser(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find user with id {}", userId, e);
        }
        return Optional.empty();
    }

    private User buildUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(ID_COLUMN));
        user.setLogin(resultSet.getString(LOGIN_COLUMN));
        user.setPassword(resultSet.getString(PASSWORD_COLUMN));
        user.setFirstName(resultSet.getString(FIRST_NAME_COLUMN));
        user.setLastName(resultSet.getString(LAST_NAME_COLUMN));
        user.setEnabled(resultSet.getBoolean(ENABLED_COLUMN));
        return user;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM user WHERE login = ?")) {
            ps.setString(1, login);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildUser(rs));
                }
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find user with login '{}'", login, e);
        }
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement();
             ResultSet rs = statement.executeQuery("SELECT * FROM user")) {
            while (rs.next()) {
                users.add(buildUser(rs));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot find all users", e);
        }
        return users;
    }

    @Override
    public User save(User user) {
        if (user.isNew()) {
            return create(user);
        } else {
            return update(user);
        }
    }

    private User create(User user) {
        String sql = "INSERT INTO user (login, password, first_name, last_name, enabled)"
                + " VALUES (?, ?, ?, ?, ?)";
        Optional<User> createdUser = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setValuesToPreparedStatement(ps, user);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                createdUser = findById(rs.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn("Cannot save new user", e);
        }
        return createdUser
                .orElseThrow(() -> new IllegalStateException("Cannot create user"));
    }

    private void setValuesToPreparedStatement(PreparedStatement ps, User user) throws SQLException {
        ps.setString(1, user.getLogin());
        ps.setString(2, user.getPassword());
        ps.setString(3, user.getFirstName());
        ps.setString(4, user.getLastName());
        ps.setBoolean(5, user.isEnabled());
    }

    private User update(User user) {
        String sql = "UPDATE user SET login = ?, password = ?, first_name = ?, last_name = ?, enabled = ?"
                + " WHERE id = ?";
        Optional<User> updatedUser = Optional.empty();
        try (Connection con = dataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            setValuesToPreparedStatement(ps, user);
            ps.setInt(6, user.getId());
            ps.executeUpdate();
            updatedUser = findById(user.getId());
        } catch (SQLException e) {
            LOG.warn("Cannot update user with id {}", user.getId(), e);
        }
        return updatedUser
                .orElseThrow(() -> new IllegalStateException("Cannot update user"));
    }
}
