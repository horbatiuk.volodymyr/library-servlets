package horbatiuk.library.repository;

import horbatiuk.library.entity.LoanType;

import java.util.Optional;

public interface LoanTypeRepository extends GenericRepository<LoanType> {

    Optional<LoanType> findByName(String name);
}
