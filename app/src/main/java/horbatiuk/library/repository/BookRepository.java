package horbatiuk.library.repository;

import horbatiuk.library.entity.Book;
import horbatiuk.library.service.PageRequest;

import java.util.Optional;

public interface BookRepository extends GenericRepository<Book> {

    @Override
    Optional<Book> findById(int bookId);

    Page<Book> findByTitleContainingOrAuthorContaining(String partOfTitle,
                                                       String partOfAuthor,
                                                       PageRequest pageRequest);

    Page<Book> findAll(PageRequest pageRequest);
}
