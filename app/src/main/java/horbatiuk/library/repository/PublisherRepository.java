package horbatiuk.library.repository;

import horbatiuk.library.entity.Publisher;

public interface PublisherRepository extends GenericRepository<Publisher> {
}
