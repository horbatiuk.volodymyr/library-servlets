package horbatiuk.library.dto;

public class BookCopyDto extends BaseDto {

    private BookDto book;
    private String inventoryNum;

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public String getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(String inventoryNum) {
        this.inventoryNum = inventoryNum;
    }
}
