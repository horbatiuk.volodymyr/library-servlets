package horbatiuk.library.dto;

import java.time.LocalDate;

public class BookLoanDto extends BaseDto {

    private String bookTitle;
    private String bookAuthor;
    private LoanTypeDto loanType;
    private String bookInventoryNum;
    private LocalDate issueDate;
    private LocalDate dueDate;
    private LocalDate returnDate;
    private int totalOverdueFine;

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public LoanTypeDto getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanTypeDto loanType) {
        this.loanType = loanType;
    }

    public String getBookInventoryNum() {
        return bookInventoryNum;
    }

    public void setBookInventoryNum(String bookInventoryNum) {
        this.bookInventoryNum = bookInventoryNum;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public int getTotalOverdueFine() {
        return totalOverdueFine;
    }

    public void setTotalOverdueFine(int totalOverdueFine) {
        this.totalOverdueFine = totalOverdueFine;
    }
}
