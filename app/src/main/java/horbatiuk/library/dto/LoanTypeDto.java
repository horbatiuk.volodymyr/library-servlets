package horbatiuk.library.dto;

public class LoanTypeDto extends BaseDto {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
