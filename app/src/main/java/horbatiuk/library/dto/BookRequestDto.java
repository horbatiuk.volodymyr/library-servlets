package horbatiuk.library.dto;

public class BookRequestDto extends BaseDto {

    private boolean satisfied;
    private BookDto book;
    private LoanTypeDto loanType;
    private UserDto user;

    public boolean isSatisfied() {
        return satisfied;
    }

    public void setSatisfied(boolean satisfied) {
        this.satisfied = satisfied;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public LoanTypeDto getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanTypeDto loanType) {
        this.loanType = loanType;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
