package horbatiuk.library.controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

import static horbatiuk.library.controller.filter.CharacterEncodingFilter.REQUEST_ENCODING;
import static horbatiuk.library.controller.filter.CharacterEncodingFilter.UTF_8;

/**
 * Этот фильтр гарантирует, что если браузер не установил кодировку,
 * используемую в запросе, то будет установлена UTF-8.
 * Еще одна вещь, которую делает этот фильтр,
 * — это установка кодировки ответа по умолчанию,
 * т.е. кодировка, в которой возвращается html или что-то другое.
 * Альтернативой является установка кодировки ответа и т. д.
 * в каждом контроллере приложения.
 */
@WebFilter(urlPatterns = "/*",
        initParams = @WebInitParam(name = REQUEST_ENCODING, value = UTF_8))
public class CharacterEncodingFilter implements Filter {

    public static final String REQUEST_ENCODING = "requestEncoding";
    public static final String UTF_8 = "UTF-8";
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";

    String encoding;

    @Override
    public void init(FilterConfig filterConfig) {
        encoding = filterConfig.getInitParameter(REQUEST_ENCODING);
        if (encoding == null) {
            encoding = UTF_8;
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // Устанавливаем кодировку клиента только тогда, когда она не указана.
        // В противном случае не меняем кодировку клиента.
        // См. пункт 3.4.1 спецификации протокола HTTP
        // https://tools.ietf.org/html/rfc2616#section-3.4.1
        if (request.getCharacterEncoding() == null) {
            request.setCharacterEncoding(encoding);
        }

        // Устанавливаем для ответа тип контента и кодировку
        response.setContentType(CONTENT_TYPE);
        response.setCharacterEncoding(UTF_8);

        chain.doFilter(request, response);
    }
}
