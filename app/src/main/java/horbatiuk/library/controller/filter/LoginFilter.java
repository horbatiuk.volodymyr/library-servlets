package horbatiuk.library.controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static horbatiuk.library.config.Constants.*;

@WebFilter(urlPatterns = {"/admin/*", "/books/*", "/librarian/*", "/users/*"})
public class LoginFilter implements Filter {

    // https://stackoverflow.com/questions/1945377/authenticating-the-username-password-by-using-filters-in-java-contacting-with
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);
        boolean loggedIn = session != null && session.getAttribute(USER_ATTRIBUTE) != null;

        String loginUrl = request.getContextPath() + LOGIN_PAGE;
        boolean loginRequest = request.getRequestURI().equals(loginUrl);

        String requestUri = request.getRequestURI();

        if (loggedIn || loginRequest) {
            chain.doFilter(request, response);
        } else {
            HttpSession newSession = request.getSession();
            newSession.setAttribute(ORIGINAL_REQUEST_URI, requestUri);
            response.sendRedirect(loginUrl);
        }
    }
}
