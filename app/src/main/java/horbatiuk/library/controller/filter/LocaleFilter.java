package horbatiuk.library.controller.filter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class LocaleFilter implements Filter {

    private static final String LOCALE_PARAM = "locale";

    // https://phrase.com/blog/posts/internationalization-basic-jsp-servlet/
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String localeValueFromRequest = request.getParameter(LOCALE_PARAM);
        if (StringUtils.isNotBlank(localeValueFromRequest)) {
            HttpSession session = ((HttpServletRequest) request).getSession();
            session.setAttribute(LOCALE_PARAM, localeValueFromRequest);
        }
        chain.doFilter(request, response);
    }
}
