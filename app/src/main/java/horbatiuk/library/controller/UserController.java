package horbatiuk.library.controller;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.BookLoanDto;
import horbatiuk.library.dto.BookRequestDto;
import horbatiuk.library.service.BookLoanService;
import horbatiuk.library.service.BookRequestService;
import horbatiuk.library.service.ServiceFactory;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static horbatiuk.library.config.Constants.PAGES;

@WebServlet("/users/*")
public class UserController extends HttpServlet {

    public static final String USERS_DIR = PAGES + "/users/";

    private BookRequestService bookRequestService;
    private BookLoanService bookLoanService;

    @Override
    public void init() {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        bookRequestService = Objects.requireNonNull(serviceFactory.getBookRequestService());
        bookLoanService = Objects.requireNonNull(serviceFactory.getBookLoanService());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] pathParts = Utils.getPathParts(request);
        if (pathParts.length != 3) {
            throw new IllegalArgumentException("The following URL format is expected: "
                    + request.getServletPath() + "/{user_id}/requests");
        }

        int userId = getUserId(pathParts[1]);
        int bookId = Utils.getParameterAsInt(request, "book_id");
        String loanTypeName = Utils.getParameterAsString(request, "loan_type_name");

        BookRequestDto bookRequest = bookRequestService.createRequest(userId, bookId, loanTypeName);
        request.setAttribute("bookRequest", bookRequest);
        response.sendRedirect("/users/" + userId + "/requests/" + bookRequest.getId());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] pathParts = Utils.getPathParts(request);

        String path;
        int userId;
        switch (pathParts.length) {
            case 2:
                userId = getUserId(pathParts[1]);
                List<BookLoanDto> bookLoans = bookLoanService.findLoaned(userId);
                request.setAttribute("bookLoans", bookLoans);
                path = USERS_DIR + "index.jsp";
                break;
            case 4:
                if (!StringUtils.equalsIgnoreCase(pathParts[2], "requests")) {
                    throw new IllegalArgumentException("Expected URL must be "
                            + "/{user_id}/requests/{book_request_id}");
                }
                int bookRequestId = getBookRequestId(pathParts[3]);
                BookRequestDto bookRequest = bookRequestService.findById(bookRequestId);
                request.setAttribute("bookRequest", bookRequest);
                path = USERS_DIR + "/request.jsp";
                break;
            default:
                throw new IllegalArgumentException("The following URL formats are expected: "
                        + request.getServletPath() + "/{user_id} or "
                        + request.getServletPath() + "/{user_id}/requests/{book_request_id}");
        }
        request.getRequestDispatcher(path).forward(request, response);
    }

    private Integer getUserId(String pathPart) {
        return getId(pathPart, "user_id");
    }

    private Integer getBookRequestId(String pathPart) {
        return getId(pathPart, "book_request_id");
    }

    private Integer getId(String pathPart, String pathVariableName) {
        return Optional.ofNullable(pathPart)
                .map(Integer::parseInt)
                .orElseThrow(() -> new IllegalArgumentException(pathVariableName
                        + " path variable is required"));
    }
}
