package horbatiuk.library.controller;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.BookDto;
import horbatiuk.library.repository.Page;
import horbatiuk.library.service.BookSearchService;
import horbatiuk.library.service.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static horbatiuk.library.config.Constants.PAGE_SIZE_PROPERTY;
import static horbatiuk.library.config.Constants.SORT_BY_PROPERTY;

@WebServlet(urlPatterns = {"", "/index"})
public class HomePageController extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger();
    private static final String EMPTY_STRING = "";
    private static final String QUERY = "query";
    private static final String SORT_BY = "sort_by";
    private static final String PAGE_SIZE = "page_size";
    private static final String REQUIRED_PAGE = "required_page";
    private static final String SEARCH_PARAMS = "searchParams";
    private static final String PAGE_NUMBERS = "page_numbers";

    private BookSearchService bookSearchService;
    private SortBy sortByDefault;
    private int pageSizeDefault;

    @Override
    public void init() {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        this.bookSearchService = Objects.requireNonNull(serviceFactory.getBookSearchService());

        String sortByAsString = Registry.getProperty(SORT_BY_PROPERTY)
                .orElseThrow(() -> new IllegalStateException("Cannot find default value for sort_by option"));
        this.sortByDefault = SortBy.valueOf(sortByAsString.toUpperCase());

        String pageSizeAsString = Registry.getProperty(PAGE_SIZE_PROPERTY)
                .orElseThrow(() -> new IllegalStateException("Cannot find default value for sort_by option"));
        this.pageSizeDefault = Integer.parseInt(pageSizeAsString);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, Object> searchParams = new HashMap<>();

        String query = Optional.ofNullable(request.getParameter(QUERY))
                .orElse(EMPTY_STRING);
        searchParams.put(QUERY, query);

        SortBy sortBy = Optional.ofNullable(request.getParameter(SORT_BY))
                .map(sortByAsString -> SortBy.valueOf(sortByAsString.toUpperCase()))
                .orElse(sortByDefault);
        searchParams.put(SORT_BY, sortBy);

        Integer pageSize = Optional.ofNullable(request.getParameter(PAGE_SIZE))
                .map(Integer::parseInt)
                .orElse(pageSizeDefault);
        searchParams.put(PAGE_SIZE, pageSize);

        Integer requiredPage = Optional.ofNullable(request.getParameter(REQUIRED_PAGE))
                .map(Integer::parseInt)
                .orElse(1);
        searchParams.put(REQUIRED_PAGE, requiredPage);

        Page<BookDto> bookPage = bookSearchService.find(query, sortBy, pageSize, requiredPage);
        List<BookDto> books = bookPage.getContent();
        request.setAttribute("books", books);
        LOG.info("Books from database: {}", books);

        int totalPages = bookPage.getTotalPages();
        List<Integer> pageNumbers = Collections.emptyList();
        if (totalPages > 0) {
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }
        searchParams.put(PAGE_NUMBERS, pageNumbers);
        request.setAttribute(SEARCH_PARAMS, searchParams);

        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
