package horbatiuk.library.controller;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.BookRequestDto;
import horbatiuk.library.service.BookRequestService;
import horbatiuk.library.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static horbatiuk.library.config.Constants.PAGES;

@WebServlet("/librarian/*")
public class LibrarianController extends HttpServlet {

    public static final String LIBRARIAN_INDEX_JSP = PAGES + "/librarian/index.jsp";
    private BookRequestService bookRequestService;

    @Override
    public void init() {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        bookRequestService = Objects.requireNonNull(serviceFactory.getBookRequestService());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<BookRequestDto> openRequests = bookRequestService.findOpen();
        request.setAttribute("openRequests", openRequests);
        request.getRequestDispatcher(LIBRARIAN_INDEX_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int requestId = Utils.getParameterAsInt(request, "request_id");
        bookRequestService.satisfy(requestId);
        response.sendRedirect("/librarian/");
    }
}
