package horbatiuk.library.controller;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.BookCopyDto;
import horbatiuk.library.service.BookCopyService;
import horbatiuk.library.service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static horbatiuk.library.config.Constants.PAGES;

@WebServlet("/books/*")
public class BookController extends HttpServlet {

    private static final String BOOKS_DIR = PAGES + "/books/";

    private BookCopyService bookCopyService;

    @Override
    public void init() {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        bookCopyService = Objects.requireNonNull(serviceFactory.getBookCopyService());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] pathParts = Utils.getPathParts(request);
        if (pathParts.length != 3) {
            throw new IllegalArgumentException("The following URL format is expected: "
                    + request.getServletPath() + "/{book_id}/copies");
        }

        int bookId = Optional.ofNullable(pathParts[1])
                .map(Integer::parseInt)
                .orElseThrow(() -> new IllegalArgumentException("book_id path variable is required"));
        boolean available = Utils.getParameterAsBooleanOrDefault(request, "available", false);

        List<BookCopyDto> bookCopies;
        if (available) {
            bookCopies = bookCopyService.findAvailable(bookId);
        } else {
            bookCopies = bookCopyService.findAllBy(bookId);
        }
        request.setAttribute("bookCopies", bookCopies);
        request.getRequestDispatcher(BOOKS_DIR + "copies.jsp").forward(request, response);
    }
}
