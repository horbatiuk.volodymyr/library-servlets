package horbatiuk.library.controller;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.UserDto;
import horbatiuk.library.service.ServiceFactory;
import horbatiuk.library.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import static horbatiuk.library.config.Constants.*;

@WebServlet("/login")
public class LoginController extends HttpServlet {

    private UserService userService;

    @Override
    public void init() {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        userService = Objects.requireNonNull(serviceFactory.getUserService());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(LOGIN_PAGE).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Optional<UserDto> userDtoOptional = userService.findByLoginAndPassword(username, password);

        if (userDtoOptional.isPresent()) {
            HttpSession session = request.getSession();
            UserDto userDto = userDtoOptional.get();
            session.setAttribute(USER_ATTRIBUTE, userDto);
            response.sendRedirect(HOME_PAGE);
        } else {
            request.setAttribute("loginError", true);
            request.getRequestDispatcher(LOGIN_PAGE).forward(request, response);
        }
    }
}
