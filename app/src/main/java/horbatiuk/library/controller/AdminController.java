package horbatiuk.library.controller;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.UserDto;
import horbatiuk.library.service.ServiceFactory;
import horbatiuk.library.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static horbatiuk.library.config.Constants.PAGES;

@WebServlet("/admin/*")
public class AdminController extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger();
    public static final String ADMIN_INDEX_JSP = PAGES + "/admin/index.jsp";

    private UserService userService;

    @Override
    public void init() {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        userService = Objects.requireNonNull(serviceFactory.getUserService());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<UserDto> users = userService.findAll();
        request.setAttribute("users", users);
        request.getRequestDispatcher(ADMIN_INDEX_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int userId = Utils.getParameterAsInt(request, "user_id");
        boolean enabled = Utils.getParameterAsBoolean(request, "enabled");

        UserDto user = userService.updateUserStatus(userId, enabled);
        LOG.info("User '{} {}' got 'enabled' flag as '{}'",
                user.getFirstName(), user.getLastName(), enabled);
        response.sendRedirect("/admin/");
    }
}
