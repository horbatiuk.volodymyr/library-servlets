package horbatiuk.library.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/simulate-exception")
public class ExceptionController extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        LOG.error("Now exception will be thrown.");
        throw new RuntimeException("This exception was thrown intentionally.");
    }
}
