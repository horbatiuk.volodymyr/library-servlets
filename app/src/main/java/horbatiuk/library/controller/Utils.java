package horbatiuk.library.controller;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.function.Supplier;

public class Utils {

    public static int getParameterAsInt(HttpServletRequest request, String name) {
        return Optional.ofNullable(request.getParameter(name))
                .map(Integer::parseInt)
                .orElseThrow(buildException(name));
    }

    public static String getParameterAsString(HttpServletRequest request, String name) {
        return Optional.ofNullable(request.getParameter(name))
                .orElseThrow(buildException(name));
    }

    public static boolean getParameterAsBoolean(HttpServletRequest request, String name) {
        return Optional.ofNullable(request.getParameter(name))
                .map(Boolean::parseBoolean)
                .orElseThrow(buildException(name));
    }

    public static boolean getParameterAsBooleanOrDefault(HttpServletRequest request,
                                                         String name, boolean defaultValue) {
        return Optional.ofNullable(request.getParameter(name))
                .map(Boolean::parseBoolean)
                .orElse(defaultValue);
    }

    private static Supplier<IllegalArgumentException> buildException(String name) {
        return () -> new IllegalArgumentException("Request doesn't contain '" + name + "' parameter");
    }

    public static String[] getPathParts(HttpServletRequest request) {
        String pathInfo = Optional.ofNullable(request.getPathInfo())
                .orElseThrow(() -> new IllegalArgumentException("There is no mapping for "
                        + request.getServletPath()));
        return pathInfo.split("/");
    }
}
