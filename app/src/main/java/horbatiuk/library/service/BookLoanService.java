package horbatiuk.library.service;

import horbatiuk.library.dto.BookLoanDto;
import horbatiuk.library.entity.BookLoan;

import java.util.List;

public interface BookLoanService {

    List<BookLoanDto> findLoaned(int userId);

    BookLoanDto save(BookLoan bookLoan);
}
