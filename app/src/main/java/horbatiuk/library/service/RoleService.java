package horbatiuk.library.service;

import horbatiuk.library.dto.RoleDto;

import java.util.List;

public interface RoleService {

    RoleDto findById(int roleId);

    List<RoleDto> findByUserId(int userId);
}
