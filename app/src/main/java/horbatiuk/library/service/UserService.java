package horbatiuk.library.service;

import horbatiuk.library.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface UserService {

    UserDto findById(int userId);

    Optional<UserDto> findByLoginAndPassword(String login, String password);

    List<UserDto> findAll();

    UserDto updateUserStatus(int userId, boolean enabled);

    UserDto save(UserDto user);
}
