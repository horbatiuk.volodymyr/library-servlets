package horbatiuk.library.service;

import horbatiuk.library.dto.BookCopyDto;

import java.util.List;

public interface BookCopyService {

    BookCopyDto findById(int bookCopyId);

    List<BookCopyDto> findAllBy(int bookId);

    List<BookCopyDto> findAvailable(int bookId);
}
