package horbatiuk.library.service;

import horbatiuk.library.controller.SortBy;

import java.util.Objects;

public class PageRequest {

    private final int page;
    private final int size;
    private final SortBy sortBy;

    public PageRequest(int page, int size, SortBy sortBy) {
        this.page = page;
        this.size = size;
        this.sortBy = Objects.requireNonNull(sortBy);
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public SortBy getSortBy() {
        return sortBy;
    }
}
