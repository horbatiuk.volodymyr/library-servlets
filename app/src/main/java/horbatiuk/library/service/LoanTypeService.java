package horbatiuk.library.service;

import horbatiuk.library.dto.LoanTypeDto;

public interface LoanTypeService {

    LoanTypeDto findById(int loanTypeId);

    LoanTypeDto findByName(String name);
}
