package horbatiuk.library.service.impl;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.BookCopyDto;
import horbatiuk.library.dto.BookDto;
import horbatiuk.library.dto.BookLoanDto;
import horbatiuk.library.dto.LoanTypeDto;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.repository.BookLoanRepository;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.BookCopyService;
import horbatiuk.library.service.BookLoanService;
import horbatiuk.library.service.LoanTypeService;
import horbatiuk.library.service.ServiceFactory;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static horbatiuk.library.config.Constants.OVERDUE_FINE_PROPERTY;

public class BookLoanServiceImpl implements BookLoanService {

    private final LoanTypeService loanTypeService;
    private final BookCopyService bookCopyService;
    private final BookLoanRepository bookLoanRepository;
    private final int overdueFine;

    public BookLoanServiceImpl(RepositoryFactory repositoryFactory) {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        loanTypeService = Objects.requireNonNull(serviceFactory.getLoanTypeService());
        bookCopyService = Objects.requireNonNull(serviceFactory.getBookCopyService());
        bookLoanRepository = Objects.requireNonNull(repositoryFactory.getBookLoanRepository());

        int overdueFine = Registry.getProperty(OVERDUE_FINE_PROPERTY)
                .map(Integer::parseInt)
                .orElseThrow(() ->
                        new IllegalStateException("Cannot find value for '" + OVERDUE_FINE_PROPERTY + "' property"));
        if ((overdueFine < 1)) {
            throw new IllegalStateException("Overdue fine must be > 0, but in fact: " + overdueFine);
        }
        this.overdueFine = overdueFine;
    }

    @Override
    public List<BookLoanDto> findLoaned(int userId) {
        List<horbatiuk.library.entity.BookLoan> bookLoans = bookLoanRepository.findByUserId(userId);
        return bookLoans.stream()
                .map(this::toBookLoanDto)
                .collect(Collectors.toList());
    }

    private BookLoanDto toBookLoanDto(horbatiuk.library.entity.BookLoan bookLoan) {
        BookLoanDto bookLoanDto = new BookLoanDto();

        bookLoanDto.setId(bookLoan.getId());
        int bookCopyId = bookLoan.getBookCopyId();
        BookCopyDto bookCopy = bookCopyService.findById(bookCopyId);

        BookDto book = bookCopy.getBook();
        bookLoanDto.setBookTitle(book.getTitle());
        bookLoanDto.setBookAuthor(book.getAuthor());

        int loanTypeId = bookLoan.getLoanTypeId();
        LoanTypeDto loanType = loanTypeService.findById(loanTypeId);
        bookLoanDto.setLoanType(loanType);

        bookLoanDto.setBookInventoryNum(bookCopy.getInventoryNum());
        bookLoanDto.setIssueDate(bookLoan.getIssueDate());
        LocalDate dueDate = bookLoan.getDueDate();
        bookLoanDto.setDueDate(dueDate);
        LocalDate returnDate = bookLoan.getReturnDate();
        bookLoanDto.setReturnDate(returnDate);

        LocalDate dateForOverdueFineCalculation = returnDate;
        if (returnDate == null) {
            dateForOverdueFineCalculation = LocalDate.now();
        }
        if (dateForOverdueFineCalculation.isAfter(dueDate)) {
            long daysBetween = ChronoUnit.DAYS.between(dueDate, dateForOverdueFineCalculation);
            bookLoanDto.setTotalOverdueFine((int) (daysBetween * overdueFine));
        }
        return bookLoanDto;
    }

    @Override
    public BookLoanDto save(BookLoan bookLoan) {
        return toBookLoanDto(bookLoanRepository.save(bookLoan));
    }
}
