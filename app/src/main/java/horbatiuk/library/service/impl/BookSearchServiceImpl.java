package horbatiuk.library.service.impl;

import horbatiuk.library.config.Registry;
import horbatiuk.library.controller.SortBy;
import horbatiuk.library.dto.BookDto;
import horbatiuk.library.dto.PublisherDto;
import horbatiuk.library.entity.Book;
import horbatiuk.library.repository.BookRepository;
import horbatiuk.library.repository.Page;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.BookSearchService;
import horbatiuk.library.service.PageRequest;
import horbatiuk.library.service.PublisherService;
import horbatiuk.library.service.ServiceFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BookSearchServiceImpl implements BookSearchService {

    private final PublisherService publisherService;
    private final BookRepository bookRepository;

    public BookSearchServiceImpl(RepositoryFactory repositoryFactory) {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        publisherService = Objects.requireNonNull(serviceFactory.getPublisherService());
        bookRepository = Objects.requireNonNull(repositoryFactory.getBookRepository());
    }

    @Override
    public BookDto findById(int bookId) {
        return bookRepository.findById(bookId)
                .map(this::toBookDto)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find book with id: " + bookId));
    }

    @Override
    public Page<BookDto> find(String query, SortBy sortBy, int pageSize, int requiredPage) {
        int page = requiredPage - 1;
        PageRequest pageRequest = new PageRequest(page, pageSize, sortBy);
        if (StringUtils.isBlank(query)) {
            return toBookDtoPage(bookRepository.findAll(pageRequest));
        } else {
            return toBookDtoPage(bookRepository.findByTitleContainingOrAuthorContaining(query, query, pageRequest));
        }
    }

    private Page<BookDto> toBookDtoPage(Page<Book> bookPage) {
        List<BookDto> bookDtos = bookPage.getContent().stream()
                .map(this::toBookDto)
                .collect(Collectors.toList());
        int totalPages = bookPage.getTotalPages();
        return new Page<>(bookDtos, totalPages);
    }

    private BookDto toBookDto(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setTitle(book.getTitle());
        bookDto.setAuthor(book.getAuthor());
        bookDto.setYear(book.getYear());
        PublisherDto publisherDto = publisherService.findById(book.getPublisherId());
        bookDto.setPublisher(publisherDto);
        return bookDto;
    }
}
