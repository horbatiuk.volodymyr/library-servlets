package horbatiuk.library.service.impl;

import horbatiuk.library.dto.PublisherDto;
import horbatiuk.library.entity.Publisher;
import horbatiuk.library.repository.PublisherRepository;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.PublisherService;

import java.util.Objects;

public class PublisherServiceImpl implements PublisherService {

    private final PublisherRepository publisherRepository;

    public PublisherServiceImpl(RepositoryFactory repositoryFactory) {
        publisherRepository = Objects.requireNonNull(repositoryFactory.getPublisherRepository());
    }

    @Override
    public PublisherDto findById(int publisherId) {
        return publisherRepository.findById(publisherId)
                .map(this::toPublisherDto)
                .orElse(new PublisherDto());
    }

    private PublisherDto toPublisherDto(Publisher publisher) {
        PublisherDto publisherDto = new PublisherDto();
        publisherDto.setId(publisher.getId());
        publisherDto.setName(publisher.getName());
        return publisherDto;
    }
}
