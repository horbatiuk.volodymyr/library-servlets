package horbatiuk.library.service.impl;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.*;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.entity.BookRequest;
import horbatiuk.library.repository.BookRequestRepository;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.*;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static horbatiuk.library.config.Constants.LOAN_TERM_DAYS_PROPERTY;

public class BookRequestServiceImpl implements BookRequestService {

    private final UserService userService;
    private final BookCopyService bookCopyService;
    private final BookSearchService bookSearchService;
    private final BookLoanService bookLoanService;
    private final LoanTypeService loanTypeService;
    private final BookRequestRepository bookRequestRepository;
    private final int loanTermDays;

    public BookRequestServiceImpl(RepositoryFactory repositoryFactory) {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        userService = Objects.requireNonNull(serviceFactory.getUserService());
        bookCopyService = Objects.requireNonNull(serviceFactory.getBookCopyService());
        bookSearchService = Objects.requireNonNull(serviceFactory.getBookSearchService());
        bookLoanService = Objects.requireNonNull(serviceFactory.getBookLoanService());
        loanTypeService = Objects.requireNonNull(serviceFactory.getLoanTypeService());

        bookRequestRepository = Objects.requireNonNull(repositoryFactory.getBookRequestRepository());

        int loanTermDays = Registry.getProperty(LOAN_TERM_DAYS_PROPERTY)
                .map(Integer::parseInt)
                .orElseThrow(() ->
                        new IllegalStateException("Cannot find value for '" + LOAN_TERM_DAYS_PROPERTY + "' property"));
        if ((loanTermDays < 1)) {
            throw new IllegalStateException("Loan term must be > 0, but in fact: " + loanTermDays);
        }
        this.loanTermDays = loanTermDays;
    }

    @Override
    public BookRequestDto createRequest(int userId, int bookId, String loanTypeName) {
        BookRequest bookRequest = new BookRequest();
        UserDto userDto = userService.findById(userId);
        bookRequest.setUserId(userDto.getId());

        BookDto book = bookSearchService.findById(bookId);
        bookRequest.setBookId(book.getId());

        LoanTypeDto loanType = loanTypeService.findByName(loanTypeName);
        bookRequest.setLoanTypeId(loanType.getId());

        return toBookRequestDto(bookRequestRepository.save(bookRequest));
    }

    private BookRequestDto toBookRequestDto(BookRequest bookRequest) {
        BookRequestDto requestDto = new BookRequestDto();
        requestDto.setId(bookRequest.getId());
        requestDto.setSatisfied(bookRequest.isSatisfied());
        requestDto.setBook(bookSearchService.findById(bookRequest.getBookId()));
        requestDto.setLoanType(loanTypeService.findById(bookRequest.getLoanTypeId()));
        requestDto.setUser(userService.findById(bookRequest.getUserId()));
        return requestDto;
    }

    @Override
    public BookRequestDto findById(int bookRequestId) {
        return bookRequestRepository.findById(bookRequestId)
                .map(this::toBookRequestDto)
                .orElseThrow(cannotFindBookRequestWithId(bookRequestId));
    }

    private Supplier<IllegalArgumentException> cannotFindBookRequestWithId(int bookRequestId) {
        return () -> new IllegalArgumentException("Cannot find bookRequest with id: " + bookRequestId);
    }

    @Override
    public List<BookRequestDto> findOpen() {
        return bookRequestRepository.findBySatisfied(false).stream()
                .map(this::toBookRequestDto)
                .collect(Collectors.toList());
    }

    @Override
    public BookRequestDto satisfy(int bookRequestId) {
        BookRequest bookRequest = bookRequestRepository.findById(bookRequestId)
                .orElseThrow(cannotFindBookRequestWithId(bookRequestId));
        bookRequest.setSatisfied(true);

        BookLoan bookLoan = new BookLoan();
        int bookId = bookRequest.getBookId();
        BookCopyDto availableBookCopy = getAvailableCopyFor(bookId);
        bookLoan.setBookCopyId(availableBookCopy.getId());
        bookLoan.setLoanTypeId(bookRequest.getLoanTypeId());
        bookLoan.setUserId(bookRequest.getUserId());
        bookLoan.setIssueDate(LocalDate.now());

        int loanTypeId = bookRequest.getLoanTypeId();
        LoanTypeDto loanTypeDto = loanTypeService.findById(loanTypeId);
        bookLoan.setDueDate(calculateDueDate(loanTypeDto));
        bookLoanService.save(bookLoan);
        return toBookRequestDto(bookRequestRepository.save(bookRequest));
    }

    private LocalDate calculateDueDate(LoanTypeDto loanTypeDto) {
        String loanTypeName = loanTypeDto.getName();
        if (StringUtils.isBlank(loanTypeName)) {
            throw new IllegalStateException("Loan type with id '"
                    + loanTypeDto.getId() + "' doesn't have non-empty name");
        }
        LocalDate now = LocalDate.now();
        switch (loanTypeName) {
            case "READING_ROOM":
                return now;
            case "HOME":
                return now.plusDays(loanTermDays);
            default:
                throw new IllegalStateException("Unknown loan type '" + loanTypeName
                        + "'. Only 'READING_ROOM' and 'HOME' are allowed.");
        }
    }

    private BookCopyDto getAvailableCopyFor(int bookId) {
        List<BookCopyDto> availableCopies = bookCopyService.findAvailable(bookId);
        return availableCopies.stream()
                .findFirst()
                .orElseThrow(() ->
                        new IllegalStateException("There are no available copies for bookId: " + bookId));
    }
}
