package horbatiuk.library.service.impl;

import horbatiuk.library.dto.LoanTypeDto;
import horbatiuk.library.entity.LoanType;
import horbatiuk.library.repository.LoanTypeRepository;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.LoanTypeService;

import java.util.Objects;

public class LoanTypeServiceImpl implements LoanTypeService {

    private final LoanTypeRepository loanTypeRepository;

    public LoanTypeServiceImpl(RepositoryFactory repositoryFactory) {
        loanTypeRepository = Objects.requireNonNull(repositoryFactory.getLoanTypeRepository());
    }

    @Override
    public LoanTypeDto findById(int loanTypeId) {
        return loanTypeRepository.findById(loanTypeId)
                .map(this::toLoanTypeDto)
                .orElseThrow(() -> new IllegalArgumentException("cannot find loan type with id: " + loanTypeId));
    }

    private LoanTypeDto toLoanTypeDto(LoanType loanType) {
        LoanTypeDto loanTypeDto = new LoanTypeDto();
        loanTypeDto.setId(loanType.getId());
        loanTypeDto.setName(loanType.getName());
        return loanTypeDto;
    }

    @Override
    public LoanTypeDto findByName(String name) {
        return loanTypeRepository.findByName(name)
                .map(this::toLoanTypeDto)
                .orElseThrow(() -> new IllegalArgumentException("cannot find loan type with name: " + name));
    }
}
