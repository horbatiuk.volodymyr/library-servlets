package horbatiuk.library.service.impl;

import horbatiuk.library.dto.RoleDto;
import horbatiuk.library.entity.Role;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.repository.RoleRepository;
import horbatiuk.library.service.RoleService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RepositoryFactory repositoryFactory) {
        roleRepository = Objects.requireNonNull(repositoryFactory.getRoleRepository());
    }

    @Override
    public RoleDto findById(int roleId) {
        return roleRepository.findById(roleId)
                .map(this::toRoleDto)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find role with id: " + roleId));
    }

    private RoleDto toRoleDto(Role role) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setName(role.getName());
        return roleDto;
    }

    @Override
    public List<RoleDto> findByUserId(int userId) {
        return roleRepository.findByUserId(userId).stream()
                .map(this::toRoleDto)
                .collect(Collectors.toList());
    }
}
