package horbatiuk.library.service.impl;

import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.BookCopyDto;
import horbatiuk.library.entity.BookCopy;
import horbatiuk.library.entity.BookLoan;
import horbatiuk.library.repository.BookCopyRepository;
import horbatiuk.library.repository.BookLoanRepository;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.BookCopyService;
import horbatiuk.library.service.BookSearchService;
import horbatiuk.library.service.ServiceFactory;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class BookCopyServiceImpl implements BookCopyService {

    private final BookSearchService bookSearchService;
    private final BookCopyRepository bookCopyRepository;
    private final BookLoanRepository bookLoanRepository;

    public BookCopyServiceImpl(RepositoryFactory repositoryFactory) {
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        bookSearchService = Objects.requireNonNull(serviceFactory.getBookSearchService());
        bookCopyRepository = Objects.requireNonNull(repositoryFactory.getBookCopyRepository());
        bookLoanRepository = Objects.requireNonNull(repositoryFactory.getBookLoanRepository());
    }

    @Override
    public BookCopyDto findById(int bookCopyId) {
        return bookCopyRepository.findById(bookCopyId)
                .map(this::toBookCopyDto)
                .orElseThrow(cannotFindBookCopyWithId(bookCopyId));
    }

    private Supplier<IllegalArgumentException> cannotFindBookCopyWithId(int bookCopyId) {
        return () -> new IllegalArgumentException("Cannot find book copy with id: " + bookCopyId);
    }

    private BookCopyDto toBookCopyDto(BookCopy bookCopy) {
        BookCopyDto bookCopyDto = new BookCopyDto();
        bookCopyDto.setId(bookCopy.getId());
        bookCopyDto.setBook(bookSearchService.findById(bookCopy.getBookId()));
        bookCopyDto.setInventoryNum(bookCopy.getInventoryNum());
        return bookCopyDto;
    }

    @Override
    public List<BookCopyDto> findAllBy(int bookId) {
        return bookCopyRepository.findByBookId(bookId).stream()
                .map(this::toBookCopyDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<BookCopyDto> findAvailable(int bookId) {
        List<BookCopy> allCopies = bookCopyRepository.findByBookId(bookId);

        List<BookLoan> bookLoans
                = bookLoanRepository.findByBookCopyInAndReturnDateIsNull(allCopies);

        List<Integer> loanedCopyIds = bookLoans.stream()
                .map(BookLoan::getBookCopyId)
                .collect(Collectors.toList());

        return allCopies.stream()
                .filter(bookCopy -> !loanedCopyIds.contains(bookCopy.getId()))
                .map(this::toBookCopyDto)
                .collect(Collectors.toList());
    }
}
