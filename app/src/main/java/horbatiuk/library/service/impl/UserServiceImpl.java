package horbatiuk.library.service.impl;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import horbatiuk.library.config.Registry;
import horbatiuk.library.dto.UserDto;
import horbatiuk.library.entity.User;
import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.repository.UserRepository;
import horbatiuk.library.service.RoleService;
import horbatiuk.library.service.ServiceFactory;
import horbatiuk.library.service.UserService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {

    private static final int ARGON2_ITERATIONS = 2;
    private static final int ARGON2_MEMORY = 16;
    private static final int ARGON2_PARALLELISM = 1;

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final Argon2 argon2;

    public UserServiceImpl(RepositoryFactory repositoryFactory) {
        userRepository = Objects.requireNonNull(repositoryFactory.getUserRepository());
        ServiceFactory serviceFactory = Registry.getServiceFactory();
        roleService = Objects.requireNonNull(serviceFactory.getRoleService());
        argon2 = Argon2Factory.create();
    }

    private UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEnabled(user.isEnabled());
        userDto.setRoles(roleService.findByUserId(user.getId()));
        return userDto;
    }

    @Override
    public UserDto findById(int userId) {
        return userRepository.findById(userId)
                .map(this::toUserDto)
                .orElseThrow(cannotFindUserWithId(userId));
    }

    private Supplier<IllegalArgumentException> cannotFindUserWithId(int userId) {
        return () -> new IllegalArgumentException("Cannot find user with id: " + userId);
    }

    @Override
    public Optional<UserDto> findByLoginAndPassword(String login, String password) {
        return userRepository.findByLogin(login)
                .filter(User::isEnabled)
                .filter(user -> isPasswordCorrect(user, password))
                .map(this::toUserDto);
    }

    private boolean isPasswordCorrect(User user, String passwordAsString) {
        char[] password = passwordAsString.toCharArray();
        boolean passwordMatches;
        try {
            String hashFromDatabase = user.getPassword();
            passwordMatches = argon2.verify(hashFromDatabase, password);
        } finally {
            argon2.wipeArray(password);
        }
        return passwordMatches;
    }

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(this::toUserDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto updateUserStatus(int userId, boolean enabled) {
        User user = userRepository.findById(userId)
                .orElseThrow(cannotFindUserWithId(userId));
        user.setEnabled(enabled);
        return toUserDto(userRepository.save(user));
    }

    @Override
    public UserDto save(UserDto userDto) {
        String passwordAsString = userDto.getPassword();
        char[] password = passwordAsString.toCharArray();
        String hashedPassword = argon2.hash(ARGON2_ITERATIONS, ARGON2_MEMORY, ARGON2_PARALLELISM, password);
        userDto.setPassword(hashedPassword);

        User savedUser = userRepository.save(toUser(userDto));
        return toUserDto(savedUser);
    }

    private User toUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEnabled(userDto.isEnabled());
        return user;
    }
}
