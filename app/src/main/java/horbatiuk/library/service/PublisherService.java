package horbatiuk.library.service;

import horbatiuk.library.dto.PublisherDto;

public interface PublisherService {

    PublisherDto findById(int publisherId);
}
