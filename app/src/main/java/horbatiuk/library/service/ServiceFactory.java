package horbatiuk.library.service;

import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.service.impl.*;

import java.util.Objects;

public class ServiceFactory {

    private final RepositoryFactory repositoryFactory;

    public ServiceFactory(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = Objects.requireNonNull(repositoryFactory);
    }

    public BookSearchService getBookSearchService() {
        return new BookSearchServiceImpl(repositoryFactory);
    }

    public BookCopyService getBookCopyService() {
        return new BookCopyServiceImpl(repositoryFactory);
    }

    public BookLoanService getBookLoanService() {
        return new BookLoanServiceImpl(repositoryFactory);
    }

    public BookRequestService getBookRequestService() {
        return new BookRequestServiceImpl(repositoryFactory);
    }

    public LoanTypeService getLoanTypeService() {
        return new LoanTypeServiceImpl(repositoryFactory);
    }

    public PublisherService getPublisherService() {
        return new PublisherServiceImpl(repositoryFactory);
    }

    public RoleService getRoleService() {
        return new RoleServiceImpl(repositoryFactory);
    }

    public UserService getUserService() {
        return new UserServiceImpl(repositoryFactory);
    }
}
