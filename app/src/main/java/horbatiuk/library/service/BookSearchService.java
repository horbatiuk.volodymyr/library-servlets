package horbatiuk.library.service;

import horbatiuk.library.controller.SortBy;
import horbatiuk.library.dto.BookDto;
import horbatiuk.library.repository.Page;

public interface BookSearchService {

    BookDto findById(int bookId);

    Page<BookDto> find(String query, SortBy sortBy, int pageSize, int requiredPage);
}
