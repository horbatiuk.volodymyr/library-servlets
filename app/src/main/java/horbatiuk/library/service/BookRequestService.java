package horbatiuk.library.service;

import horbatiuk.library.dto.BookRequestDto;

import java.util.List;

public interface BookRequestService {

    BookRequestDto createRequest(int userId, int bookId, String loanTypeName);

    BookRequestDto findById(int bookRequestId);

    List<BookRequestDto> findOpen();

    BookRequestDto satisfy(int bookRequestId);
}
