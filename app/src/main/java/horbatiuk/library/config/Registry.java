package horbatiuk.library.config;

import horbatiuk.library.repository.RepositoryFactory;
import horbatiuk.library.repository.mysql.MySqlRepositoryFactory;
import horbatiuk.library.service.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.Optional;
import java.util.Properties;

import static horbatiuk.library.config.Constants.DATABASE_TYPE_PROPERTY;

public class Registry {

    private enum DatabaseType {
        MYSQL
    }

    private static final Logger LOG = LogManager.getLogger();
    private static final Properties PROPERTIES;
    private static final EnumMap<DatabaseType, RepositoryFactory> REPOSITORY_FACTORIES;
    private static final DatabaseType DATABASE_TYPE;

    static {
        REPOSITORY_FACTORIES = new EnumMap<>(DatabaseType.class);
        REPOSITORY_FACTORIES.put(DatabaseType.MYSQL, new MySqlRepositoryFactory());

        PROPERTIES = getProperties();
        String databaseTypeAsString = PROPERTIES.getProperty(DATABASE_TYPE_PROPERTY);
        DATABASE_TYPE = DatabaseType.valueOf(databaseTypeAsString.toUpperCase());
    }

    private static Properties getProperties() {
        Properties properties = null;
        ClassLoader classLoader = Registry.class.getClassLoader();
        String propertiesFileName = "application.properties";
        try (InputStream input = classLoader.getResourceAsStream(propertiesFileName)) {
            if (input == null) {
                throw new IllegalStateException("Cannot find file " + propertiesFileName);
            }
            properties = new Properties();
            properties.load(input);
        } catch (IOException e) {
            LOG.error("Exception during reading {}", propertiesFileName, e);
        }
        return properties;
    }

    public static Optional<String> getProperty(String name) {
        return Optional.ofNullable(PROPERTIES.getProperty(name));
    }

    public static RepositoryFactory getRepositoryFactory() {
        RepositoryFactory factory = REPOSITORY_FACTORIES.get(DATABASE_TYPE);
        if (factory == null) {
            throw new IllegalStateException("There is no repository factory for database " + DATABASE_TYPE);
        }
        return factory;
    }

    public static ServiceFactory getServiceFactory() {
        return new ServiceFactory(getRepositoryFactory());
    }
}
