package horbatiuk.library.config;

public class Constants {

    public static final String DATABASE_TYPE_PROPERTY = "library.database-type";
    public static final String SORT_BY_PROPERTY = "library.sort-by";
    public static final String PAGE_SIZE_PROPERTY = "library.page-size";
    public static final String LOAN_TERM_DAYS_PROPERTY = "library.loan-term-days";
    public static final String OVERDUE_FINE_PROPERTY = "library.overdue-fine";
    public static final String USER_ATTRIBUTE = "user";
    public static final String ORIGINAL_REQUEST_URI = "original_request_uri";
    public static final String HOME_PAGE = "/";
    public static final String LOGIN_PAGE = "/login.jsp";
    public static final String PAGES = "/WEB-INF/pages";
}
