package horbatiuk.library.entity;

public class BookRequest extends BaseEntity {

    private boolean satisfied;
    private int bookId;
    private int loanTypeId;
    private int userId;

    public boolean isSatisfied() {
        return satisfied;
    }

    public void setSatisfied(boolean satisfied) {
        this.satisfied = satisfied;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getLoanTypeId() {
        return loanTypeId;
    }

    public void setLoanTypeId(int loanTypeId) {
        this.loanTypeId = loanTypeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "BookRequest{" +
                "satisfied=" + satisfied +
                ", bookId=" + bookId +
                ", loanTypeId=" + loanTypeId +
                ", userId=" + userId +
                "}";
    }
}
