package horbatiuk.library.entity;

import java.util.Objects;

public class BookCopy extends BaseEntity {

    private int bookId;
    private String inventoryNum;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(String inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookCopy bookCopy = (BookCopy) o;
        return Objects.equals(inventoryNum, bookCopy.inventoryNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inventoryNum);
    }

    @Override
    public String toString() {
        return "BookCopy{" + inventoryNum + "}";
    }
}
