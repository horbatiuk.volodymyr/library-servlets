package horbatiuk.library.controller.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.*;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CharacterEncodingFilterTest {

    private static final String REQUEST_ENCODING = "requestEncoding";
    private static final String SOME_ENCODING = "cp1251";
    private static final String UTF_8 = "UTF-8";
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";

    @Mock
    private FilterConfig filterConfig;
    @Mock
    private ServletRequest request;
    @Mock
    private ServletResponse response;
    @Mock
    private FilterChain chain;

    private CharacterEncodingFilter filter;

    @BeforeEach
    void setUp() {
        filter = new CharacterEncodingFilter();
    }

    @Test
    void shouldSetEncodingFromFilterConfig() {
        when(filterConfig.getInitParameter(REQUEST_ENCODING)).thenReturn(SOME_ENCODING);

        filter.init(filterConfig);

        assertThat(filter.encoding).isEqualTo(SOME_ENCODING);
    }

    @Test
    void shouldSetUtf8EncodingWhenFilterConfigDoesNotContainRequestEncodingParam() {
        filter.init(filterConfig);

        assertThat(filter.encoding).isEqualTo(UTF_8);
    }

    @Test
    void shouldSetUtf8ToRequestCharacterEncodingWhenRequestDoesNotHaveEncoding()
            throws ServletException, IOException {
        filter.init(filterConfig);

        filter.doFilter(request, response, chain);

        verify(request).setCharacterEncoding(UTF_8);
    }

    @Test
    void shouldNotChangeRequestCharacterEncodingWhenRequestHasIt()
            throws ServletException, IOException {
        filter.init(filterConfig);
        when(request.getCharacterEncoding()).thenReturn(SOME_ENCODING);

        filter.doFilter(request, response, chain);

        verify(request, never()).setCharacterEncoding(anyString());
    }

    @Test
    void shouldSetResponseContentType() throws ServletException, IOException {
        filter.init(filterConfig);

        filter.doFilter(request, response, chain);

        verify(response).setContentType(CONTENT_TYPE);
    }

    @Test
    void shouldSetResponseCharacterEncoding() throws ServletException, IOException {
        filter.init(filterConfig);

        filter.doFilter(request, response, chain);

        verify(response).setCharacterEncoding(UTF_8);
    }

    @Test
    void shouldCallChainDoFilterAtTheEnd() throws ServletException, IOException {
        filter.init(filterConfig);

        filter.doFilter(request, response, chain);

        InOrder inOrder = inOrder(request, response, chain);
        inOrder.verify(request).getCharacterEncoding();
        inOrder.verify(request).setCharacterEncoding(UTF_8);
        inOrder.verify(response).setContentType(CONTENT_TYPE);
        inOrder.verify(response).setCharacterEncoding(UTF_8);
        inOrder.verify(chain).doFilter(request, response);
    }
}
