# Бібліотека

## Функціональні вимоги

_Читач_ реєструється в системі і далі має можливість:

* здійснювати пошук (за автором або назвою _Книги_);
* оформляти замовлення на _Книгу_ з _Каталогу_.

_Незареєстрований читач_ не може замовити книгу.

Для каталогу реалізувати можливість сортування книг:

* за назвою;
* за автором;
* за виданням;
* за датою видання.

_Бібліотекар_ видає читачеві книгу на абонемент або в читальний зал. _Книга_ видається _Читачеві_ на певний термін. При
не поверненні книги в зазначений термін читачеві нараховується штраф.

_Книга_ може бути присутньою в бібліотеці в одному або декількох екземплярах. Система веде облік доступної кількості
книг.

Кожен користувач має особистий кабінет, в якому відображається реєстраційна інформація, а також:

1. Для _Читача_:
    * список книг, які знаходяться на абонементі і дата можливого повернення (якщо дата прострочена, відображається
      розмір штрафу);
2. Для _Бібліотекаря_:
    * список замовлень читачів;
    * список читачів та їх абонементи.

_Адміністратор_ системи володіє правами:

* додавання та видалення книги, редагування інформації про книгу;
* створення та видалення бібліотекаря;
* блокування та розблокування користувача.

## Нефункціональні вимоги

1. На основі сутностей предметної області створити класи, які їм відповідають.
1. Класи та методи повинні мати назви, що відображають їх функціональність, і повинні бути рознесені по пакетах.
1. Оформлення коду має
   відповідати [Java Code Conventions](https://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
1. Інформацію щодо предметної області зберігати у реляційній базі даних (в якості СУБД рекомендується використовувати
   MySQL або PostgreSQL).
1. Застосунок має підтримувати роботу з кирилицею (бути багатомовним), в тому числі при зберіганні інформації в базі
   даних:
    * повинна бути можливість перемикання мови інтерфейсу;
    * повинна бути підтримка введення, виведення і зберігання інформації (в базі даних), записаної на різних мовах;
    * в якості мов обрати мінімум дві: одна на основі кирилиці (українська або російська), інша на основі латиниці (
      англійська);
    * дати повинні бути реалізовані через DataTime бібліотеку (Java8).
1. Реалізувати захист від повторної відправки даних на сервер при оновленні сторінки (реалізувати PRG).
1. У застосунку повинні бути реалізовані аутентифікація і авторизація, розмежування прав доступу користувачів системи до
   компонентів програми. Шифрування паролів заохочується.
1. Впровадити у проект журнал подій із використанням бібліотеки log4j.
1. Код повинен містити коментарі документації (всі класи верхнього рівня, нетривіальні методи та конструктори).
1. Застосунок має бути покритим модульними тестами (мінімальний відсоток покриття 40%). Написання інтеграційних тестів
   заохочуються. Використання Mockito заохочується.
1. Реалізувати механізм пагінації сторінок з даними.
1. Всі поля введення повинні бути із валідацією даних.
1. Застосунок має коректно реагувати на помилки та виключні ситуації різного роду (кінцевий користувач не повинен бачити
   stack trace на стороні клієнта).
1. Самостійне розширення постановки задачі по функціональності (додавання капчі, формування звітів у різних форматах,
   тощо) заохочується!
1. Використання [HTML, CSS, JS](http://htmlbook.ru) фреймворків для інтерфейсу користувача
   ([Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/), Materialize, ін.) заохочується!
1. Розробка проектів за допомогою Git заохочується.
1. Для доступу до даних використовувати [JDBC API](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html) із
   застосуванням готового або ж розробленого самостійно пулу з'єднань. _Не_ допускається використання ORM-фреймворків.
1. Архітектура застосунка повинна відповідати шаблону MVC. _Не_ допускається використання MVC-фреймворків.
1. При реалізації бізнес-логіки необхідно використовувати
   [шаблони проектування](https://ru.wikipedia.org/wiki/Категория:Шаблоны_проектирования):
   [Команда](https://refactoring.guru/ru/design-patterns/command),
   [Стратегія](https://refactoring.guru/ru/design-patterns/strategy),
   [Фабрика](https://refactoring.guru/ru/design-patterns/abstract-factory),
   [Будівельник](https://refactoring.guru/ru/design-patterns/builder),
   [Сінглтон](https://refactoring.guru/ru/design-patterns/singleton),
   [Фронт-контролер](https://ru.wikipedia.org/wiki/Единая_точка_входа_(шаблон_проектирования)),
   [Спостерігач](https://ru.wikipedia.org/wiki/Наблюдатель_(шаблон_проектирования)),
   [Адаптер](https://refactoring.guru/ru/design-patterns/adapter) та ін. Використання шаблонів повинно бути
   обґрунтованим.
1. Використовуючи [сервлети](https://docs.oracle.com/javaee/5/tutorial/doc/bnafd.html)
   та [JSP](https://docs.oracle.com/javaee/5/tutorial/doc/bnagx.html), реалізувати функціональність, наведену в
   завданні.
1. Використовувати Apache Tomcat у якості контейнера сервлетів.
1. На сторінках JSP застосовувати теги з [бібліотеки JSTL](https://docs.oracle.com/javaee/5/tutorial/doc/bnakc.html) та
   розроблені [власні теги](https://docs.oracle.com/javaee/5/tutorial/doc/bnalj.html) (мінімум: один
   тег [custom tag library](https://www.tutorialspoint.com/jsp/jsp_custom_tags.htm) та один
   тег [tag file](https://docs.oracle.com/javaee/5/tutorial/doc/bnama.html)).
1. При розробці використовувати сесії, фільтри, слухачі.
