# REST-структура

    localhost:8081
    ├── admin GET — список всех пользователей, POST — обновление статуса пользователя
    ├── books/{book_id}
    │         └── copies GET — список книг (всех или доступных для заказа)
    ├── librarian GET — список открытых заказов, POST — выдача заказа
    ├── users/{user_id} GET — список всех полученных книг пользователя
    │         └── requests POST — создание заказа для пользователя
    │             └── {book_request_id} GET — просмотр заказа пользователя
    ├── simulate-exception GET — выбросывание исключение для демонстрации
    └── index GET — список всех книг, поиск книг по параметрам

# Запуск приложения

## Из IntelliJ IDEA

Открыть вкладку Gradle и далее:

* Для обычного запуска вызвать `library-servlets → Tasks → gretty → appRun`
* Для запуска в режиме отладки вызвать `library-servlets → Tasks → gretty → appRunDebug`
  и затем подключиться удалённым отладчиком, выбрав из меню
  `Run → Edit Configurations... → Remote JVM Debug`

Приложение будет доступно по адресу http://localhost:8081

## Из консоли

Открыть консоль в папке проекта и:

* Для обычного запуска выполнить `gradlew.bat appRun`
* Для запуска в режиме отладки выполнить `gradlew.bat appRunDebug`
  и затем подключиться удалённым отладчиком, выбрав из меню
  `Run → Edit Configurations... → Remote JVM Debug`

Приложение будет доступно по адресу http://localhost:8081

# Остановка приложения

## Из IntelliJ IDEA

Открыть вкладку Gradle и вызвать `library-servlets → Tasks → gretty → appStop`

## Из консоли

В консоли, из которой запускали приложение, нажать `Ctrl + C`

# Полезное

Для того чтобы IntelliJ IDEA редактировала `messages_ru.properties` в читаемом виде, нужно в
меню `File → Settings... → Editor → File Encodings` установить флажок
`Transparent native-to-ascii conversion`
( см. [документацию](https://www.jetbrains.com/help/idea/resource-bundle.html#editing)).

* [Как заставить UTF-8 работать в веб-приложениях Java?](https://stackoverflow.com/questions/138948/how-to-get-utf-8-working-in-java-webapps)
